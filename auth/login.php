<?php
  session_start();
  require 'src/authentication/authenticate.php';
  require __DIR__ . '/vendor/autoload.php';
  use sendotp\sendotp;

  $data_back = json_decode(file_get_contents('php://input'));
  error_log(print_r($data_back,true));
  $phnNumber = $data_back->phoneNo;
  $phnId = $data_back->phoneId;
  $jwt = $data_back->jwt;
  $otpVal = $data_back->otp;
  $userId = $data_back->userId;
  if(empty($jwt)){
    if(!empty($phnNumber) && !empty($phnId) && !empty($otpVal)) { 
    if($_SESSION[$phnNumber]+5*60 < time()){
    $otp = new sendotp( '136255AWUpnMve7ZM586e53db','Your otp is {{otp}}. Please do not share Me.');
    $result = $otp->verify($phnNumber, $otpVal);
    if( json_decode($result, true)['message']=="otp_verified"){
       $finalOut = Login::firstLogin($phnId,$phnNumber);
       echo $finalOut;
       session_unset();     // unset $_SESSION variable for the run-time 
       session_destroy();   // destroy session data in storage
      } else {
         echo json_encode(array(
          'status' => 'Fail',
          'message' => 'Invalid OTP'
          )
        );
        session_unset();     // unset $_SESSION variable for the run-time 
        session_destroy();   // destroy session data in storage
      } 
    } else {
      echo json_encode(array(
        'status' => 'Fail',
        'message' => 'Timeout'
      ));
    }
    } else {
      echo json_encode(array(
       'status' => 'Fail',
       'message' => 'phone number, phone id and OTP should not be empty.'
       )
     );
   } 
  } else {
    $rest = Login::ifLogin($jwt);
    echo $rest;
  }
?>