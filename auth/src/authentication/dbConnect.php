<?php
class MyDatabase
{
    private static $cont  = null;
// below are database configurations
    public function __construct() {
        die('Init function is not allowed');
    }
    
    public static function connect($host,$dbName,$userName,$password){
        $dbUserPassword = $password;
        if($password=="zunganman@MR123"){
        $dbUserPassword = getenv('dbUserPasswordMsql');
    } 
       // One connection through whole application
       if ( null == self::$cont )
       {     
        try
        {	
            $source = 'mysql:host='.$host.';dbname='.$dbName.';charset=utf8mb4';
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false
            ];
            self::$cont = new PDO($source, $userName, $dbUserPassword, $options);
            error_log(print_r(self::$cont,true));
        }
        catch(PDOException $e)
        {
            echo ('in exception'.$e->getMessage());
            die('unable to connect to database');
            return $e->getMessage();
        }
       }
       return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
}