<?php
require 'dbConnect.php';
require __DIR__ . '/../../vendor/autoload.php';
use \Firebase\JWT\JWT;
class Login {
    public function __construct() {
    }
    public static function firstLogin($phoneId,$phoneNo){
        $id = null;
        $config['db']['host'] = "localhost";
        $config['db']['username']  = "root";
        $config['db']['password'] = "zunganman@MR123";
        $config['db']['database'] = "zunpulse";
        $config['jwt']['key'] = 'D809526552FBF43AB6748A69FBAF99BCABD77390F1D516D9A0E355D70DC4B576';
        $conn = MyDatabase::connect($config['db']['host'],$config['db']['database'],$config['db']['username'],$config['db']['password']);
          $id = null;
          $query = $conn->prepare('SELECT *FROM users WHERE phone=:phone');
          $query->bindParam(':phone',$phoneNo);
          $query->execute();
        if($query->rowCount()>0){
          $rsl = $query->fetch(PDO::FETCH_ASSOC);
          $id = $rsl['id'];
          $qu = $conn->prepare('UPDATE login set userid=:useid WHERE phoneNo=:phoneNo');
          $qu->bindParam(':useid',$id);
          $qu->bindParam(':phoneNo',$phoneNo);
          $qu->execute();
        } else {
          $q = $conn->prepare('INSERT INTO users(phone) VALUES (:phone)');
          $q->bindParam(':phone',$phoneNo);
          $q->execute();
          $id = $conn->lastInsertId(); 
        $sql = 'INSERT INTO login (phoneNo,phoneId,userid) VALUES (:phone_no, :phone_id,:userid)';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':phone_no',$phoneId);
        $stmt->bindParam(':phone_id', $phoneNo);
        $stmt->bindParam(':userid', $id);
        $stmt->execute();   
      }
      $currTime=time();
      header('Content-Type: application/json');
        $payload = array(
          "userId" => $id,
          "phoneId" => $phoneId,
          "time"=>$currTime
      );
      $jwt = JWT::encode($payload, $config['jwt']['key']);
      $updateQ = $conn->prepare('UPDATE users SET jwt=:jwt WHERE phone=:phn');
      $updateQ->bindParam(':jwt',$jwt);
      $updateQ->bindParam(':phn',$phoneNo);
      $updateQ->execute();
      $result = array(
          'status' => '200 ok',
          'userId' => $id,
          'jwt' => $jwt,
          'message' => 'Succcess'
        ); 
      return json_encode($result);  
    }
    public static function ifLogin($jwt,$userId){ 
        $config['db']['host'] = "localhost";
        $config['db']['username']  = "root";
        $config['db']['password'] = "zunganman@MR123";
        $config['db']['database'] = "zunpulse";
        $config['jwt']['key'] = 'D809526552FBF43AB6748A69FBAF99BCABD77390F1D516D9A0E355D70DC4B576';
        try {
          header('Content-Type: application/json');
          $payload = JWT::decode($jwt, $config['jwt']['key'], array('HS256'));
          if (!$payload){ 
            return json_encode( array(
              'status' => '400 Bad Request',
              'message' => 'wrong token'
            )
          );
          }
          $unencodedData = (array) $payload;
          if($unencodedData['userId']==$userId){
          $conn = MyDatabase::connect($config['db']['host'],$config['db']['database'],$config['db']['username'],$config['db']['password']);
          $sql = 'SELECT *FROM users WHERE userid=:userid and jwt=:jwt';
          $stmt = $conn->prepare($sql);
          $stmt->bindParam(':userid',$unencodedData['userId']);
          $stmt->bindParam(':jwt',$jwt);
          $stmt->execute();
           if($stmt->rowCount()>0){
                return json_encode( array(
                  'status' => '200 ok',
                  'message'=> 'success'
                )
                );
          }
        } else {
          return json_encode( array(
            'status' => '400 Bad Request',
            'message' => 'wrong token'
          )
        );
        }
       } catch (Exception $e) {
          die(var_dump($e));
          return json_encode( array(
            'status' => '400 Bad Request',
            'message' => 'wrong token',
            'error'=>$e
          )
          );      
        }
      }
}
?>