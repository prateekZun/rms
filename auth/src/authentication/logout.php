<?php
session_start();
session_unset();
session_destroy();
header('Refresh: 0;');
$userId ="userId";
$jwt ="jwt";
if (!isset($_COOKIE[$userId]) && !isset($_COOKIE[$jwt])){
    header("location:../../../ZunPulseUi/login.php");
} else {
setcookie($userId, "", time()-60*60*24*90, '/', '', 0, 0);
unset($_COOKIE[$userId]);
setcookie($jwt, "", time()-60*60*24*90, '/', '', 0, 0);
unset($_COOKIE[$jwt]);
header("location:../../../RMSUI/login.php");
 }
?>