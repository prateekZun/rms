<?php
require 'vendor/autoload.php';
require './mongoDbConnection.php';
$url="https://api.data.gov.in/resource/3b01bcb8-0b14-4abf-b6f2-c1bfd384ba69?format=json&api-key=579b464db66ec23bdd000001b147dca9a8944d304f4d25419fe69194";
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);

$mongoConn = MyMongoDB::connect();
$collections = $mongoConn->pollutionData->govData;
$collections->insertOne(json_decode($result,TRUE));
?>
