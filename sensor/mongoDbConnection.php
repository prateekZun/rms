<?php
class MyMongoDB
{
    private static $cont  = null;
    public function __construct() {
        die('Init function is not allowed');
    }
    public static function connect(){
    $dbUsername = getenv('dbUserNameMongo');
    $dbUserPassword =getenv('dbPasswordMongo');
    if ( null == self::$cont )
    {     
        try
        {	   
          #   $connecting_string =  sprintf('mongodb://%s:%d/%s', $hosts, $port,$database);
            self::$cont = new \MongoDB\Client("mongodb://localhost:27017", array($dbUsername => $dbUserPassword));
          #  self::$cont = new MongoDB\Driver\Manager($connecting_string,array('username'=>$username,'password'=>$password));
        }
        catch (MongoDB\Driver\Exception\AuthenticationException $e) {
            echo "Exception:", $e->getMessage(), "\n";
        }
        catch (MongoDB\Driver\Exception\ConnectionException $e) {
            echo "Exception:", $e->getMessage(), "\n";
        }
        catch (MongoDB\Driver\Exception\ConnectionTimeoutException $e) {
            echo "Exception:", $e->getMessage(), "\n";
        }
    }
    return self::$cont;
 }
    public static function disconnect()
    {
        self::$cont = null;
    }
}
?>
