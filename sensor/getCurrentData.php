<?php
session_start();
require 'vendor/autoload.php';
require '../auth/src/authentication/validUser.php';
require './phpMQTT.php';
$server = "www.zunpulse.com";     // change if necessary
$port = 8883;                     // change if necessary
$username = "nodemcu1";                   // set your username
$password = "nodemcu1password";              // set your password
$client_id = "publish_uniques_realData"; // make sure this is unique for connecting to sever - you could use uniqid()
$data_back = json_decode(file_get_contents('php://input'));
$deviceId = $data_back->deviceId;
$jwt = $data_back->jwt;
$userId = $data_back->userId;
$version = $data_back->version; 
if($version!=0){
  $deviceId = "zunroof/rms/".$deviceId;
}
$valid = ValidateUser::validLogin($jwt,$userId);
error_log(print_r($deviceId,true));  
$mqtt = new phpMQTT($server, $port, $client_id,null);
if(!$mqtt->connect(true, NULL, $username, $password)) {
	exit(1);
}
if($valid){ 
$topics[$deviceId] = array("qos" => 0, "function" => "procmsg");
$mqtt->subscribe($topics, 0);
}
function procmsg($topic, $msg){
    global $returnArray;
    global $deviceId;
    $returnArray = array();
    $res_temp = explode(',', $msg); 
    global $done;
   // global $mismatch;
    foreach ($res_temp as $match) {
        $results = explode('=', $match);
        $returnArray[$results[0]] = trim($results[1]);
    }
    if(!empty($msg)){
        //$returnArray['Etoday'] = (int)$returnArray['E_kwhr']-(int)$_SESSION[$deviceId."yesterday"];
        $done = 1;
        // return returnData($returnArray);
    }
}
// function returnData($returnArray){
//     echo json_encode(array(
//         'v' =>$returnArray['v'] ,
//         'I' =>$returnArray['I'] ,
//         'P_watt' =>$returnArray['P_watt'] ,
//         'PF' =>$returnArray['PF'] ,
//         'f_hz' =>$returnArray['f_hz'] ,
//         'E_kwhr' =>$returnArray['E_kwhr'] ,
//         'message' => 'data Sent'
//     ));
// }
$start_time = time();
function hasTimedout() {
     global $start_time;
    return (time() - $start_time > 15);
}
while ($mqtt->proc() && !$done && !hasTimedout()) { }
$mqtt->close();
if(!$done){
    echo json_encode( array(
        'status' => 'Device not responding',
        'message' => 'empty',
        'error'=>0
      )
      );      
} else {
echo json_encode($returnArray);
}
?>
