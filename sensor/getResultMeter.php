<?php
require 'vendor/autoload.php';
session_start();
require './mongoDbConnection.php';
require '../auth/src/authentication/dbConnect.php';
require './phpMQTT.php';
require './helper/getSunriseAndSunset.php';
#$_SESSION['todayGen']= null;
$server = "www.zunpulse.com";     // change if necessary
$port = 8883;                     // change if necessary
$username = "nodemcu1";                   // set your username
$password = "nodemcu1password";                   // set your password
$client_id = "publish13349"; // make sure this is unique for connecting to sever - you could use uniqid()
$mqtt = new phpMQTT($server, $port, $client_id,null);
if(!$mqtt->connect(true, NULL, $username, $password)) {
	exit(1);
}

$topics['energy_meter'] = array("qos" => 0, "function" => "procmsg");
$mqtt->subscribe($topics, 0);
while ($mqtt->proc()) { }
$mqtt->close();

function procmsg($topic, $msg){
    $returnArray = array();
    $res_temp = explode(',', $msg); 
    #error_log(print_r($res_temp,true));
    foreach ($res_temp as $match) {
        $results = explode('=', $match);
        $returnArray[$results[0]] = trim($results[1]);
    }
   # $_SESSION[$returnArray['chipID']]=$returnArray;
    $mongoConn = MyMongoDB::connect(); 
    $dateTable = new DateTime();
    $resultDate=$dateTable->format('Y-m-d');
    $dbName = $returnArray['chipID']; 
     #  $_SESSION[$dbName."totalEnergy"]= $returnArray['E_kwhr'];
    // $_SESSION["nodemcu1_energy_Meter_modbus_volt"]= $returnArray['v'];
    // $_SESSION["nodemcu1_energy_Meter_modbus_current"]= $returnArray['I'];
    // $_SESSION["nodemcu1_energy_Meter_modbus_powerFact"]= $returnArray['PF'];
    // $_SESSION["nodemcu1_energy_Meter_modbus_frequency"]= $returnArray['f_hz'];
//    if(isset( $returnArray['E_kwhr_Today'])) {
//     $_SESSION[$dbName."todayEnergy"]= $returnArray['E_kwhr_Today'];
//   }   
if(date("H")=='23' && date("i")=='56' && date("i")=='57' &&date("i")=='58'
&& date("i")=='59' && !isset($_SESSION[$resultDate.$dbName])){
 if(date("Y-m-t", strtotime($resultDate))==$resultDate){
    $collectMonthlyEnergy = $mongoConn->$dbName->energy_monthly;
    $mOut->energyTotal = $returnArray['E_kwhr'];
    $mOut->month = date('M-Y');
    $collectMonthlyEnergy->insertOne($mOut);
 }
 if(date("Y")."-12-31"==$resultDate){
    $collectYearlyEnergy = $mongoConn->$dbName->energy_yearly;
    $yOut->energyTotal = $returnArray['E_kwhr'];
    $yOut->year = date('Y');
    $collectYearlyEnergy->insertOne($yOut);
 } 
 $collectionEnergy = $mongoConn->$dbName->energy;
 $_SESSION[$resultDate.$dbName]=true;
 $out->energy_total = $returnArray['E_kwhr'];
 #$_SESSION[$dbName."yesterday"]=$returnArray['E_kwhr'];
 $out->date = $resultDate;
 $collectionEnergy->insertOne($out);
}
if(date("H")=='00'){
    unset($_SESSION[$resultDate]);
}
$collections = $mongoConn->$dbName->$resultDate;
$outparam->power = $returnArray['P_watt'];
#$outparam->updated_at = new MongoDB\BSON\UTCDateTime(new DateTime);
$outparam->time = date("H").":".date("i").":".date("s");
$collections->insertOne($outparam);
sleep(80);
}
?>
