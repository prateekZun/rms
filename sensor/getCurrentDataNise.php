<?php
session_start();
require 'vendor/autoload.php';
require '../auth/src/authentication/validUser.php';
require './phpMQTT.php';
$server = "www.zunpulse.com";     // change if necessary
$port = 8883;                     // change if necessary
$username = "nodemcu1";                   // set your username
$password = "nodemcu1password";              // set your password
$client_id = "publish_nise_2133"; // make sure this is unique for connecting to sever - you could use uniqid()
$data_back = json_decode(file_get_contents('php://input'));
$deviceId = $data_back->deviceId;
$jwt = $data_back->jwt;
$userId = $data_back->userId;
$start_time = time();
$valid = ValidateUser::validLogin($jwt,$userId);
error_log(print_r($deviceId,true));  
$mqtt = new phpMQTT($server, $port, $client_id,null);
if(!$mqtt->connect(true, NULL, $username, $password)) {
	exit(1);
}
if($valid){ 
 $topics[$deviceId] = array("qos" => 0, "function" => "procmsg");
 $mqtt->subscribe($topics, 0);
}
function procmsg($topic, $msg){
    global $returnArray;
    global $done;
    $returnArray = array();
    $res_temp = explode(',', $msg); 
        foreach ($res_temp as $match) {
        $results = explode('=', $match);
        $returnArray[$results[0]] = trim($results[1]);
    }
    if(!empty($msg)){
        $done = 1;
        return;
    }
}
function hasTimedout() {
     global $start_time;
    return (time() - $start_time > 15);
}
while ($mqtt->proc() &&  !$done && !hasTimedout()) { }
$mqtt->close();
if(!isset($done) || !$done){
    echo json_encode( array(
        'status' => 'Device not responding',
        'message' => 'empty',
        'error'=>0
      )
      ); 
  exit;         
} else {
echo json_encode($returnArray);
exit;  
}
?>
