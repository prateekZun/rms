<?php
session_start();
require 'vendor/autoload.php';
require './mongoDbConnection.php';
require '../auth/src/authentication/validUser.php';
$data_back = json_decode(file_get_contents('php://input'));
error_log(print_r($data_back,true));
$jwt = $data_back->jwt;
$userId = $data_back->userId;
$deviceId = $data_back->deviceId;
$startDate = $data_back->startDate;
$endDate = $data_back->endDate;
error_log(print_r($type,true));
$valid = 0;
if(!isset($_SESSION['valid-token'])){
$valid = validateUser::validLogin($jwt,$userId);
}
if($valid || $_SESSION['valid-token']==true){
    $mongoconn = MyMongoDB::connect();
    $collection = $mongoconn->$deviceId->energy;
    $_SESSION['valid-token']=true;
    if(new DateTime($endDate) > new DateTime()){
        $endDate = (new DateTime())->format("Y-m-d");
    }
    $begin =  new DateTime($startDate);
    $begin->modify('-2 day');
    $begin = $begin->format('Y-m-d'); 
    $end = new DateTime($endDate);
    $end->modify('+1 day');
    $end = $end->format('Y-m-d'); 
    $rangeQuery = array('date' => array( '$gt'=> $begin, '$lt'=> $end  ));
    $cursor = $collection->find($rangeQuery);
    $results = array();
    foreach ( $cursor as $id => $value )
    {
       $json = MongoDB\BSON\toJSON(MongoDB\BSON\fromPHP($value));
       $json =  json_decode($json, true);
       $val = array("date"=>$json['date'],"energy_total"=>$json['energy_total']);
       if(!in_array($val, $results)){
       error_log(print_r($json,true));
       array_push($results,$val);
       }
    }
    for ($i = count($results)-1; $i >= 0; $i--) {
        $results[$i]['energy_total'] = $results[$i]['energy_total']-$results[$i-1]['energy_total'];
    }
    array_shift($results);
    echo json_encode($results);
}
?>
