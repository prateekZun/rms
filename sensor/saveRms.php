<?php
require 'vendor/autoload.php';
session_start();
require './mongoDbConnection.php';
require '../auth/src/authentication/dbConnect.php';
require './phpMQTT.php';
require './helper/getSunriseAndSunset.php';
$server = "www.zunpulse.com";     // change if necessary
$port = 8883;                     // change if necessary
$username = "nodemcu1";                   // set your username
$password = "nodemcu1password";                   // set your password
$client_id = "publish13349"; // make sure this is unique for connecting to sever - 
$mqtt = new phpMQTT($server, $port, $client_id,null);
if(!$mqtt->connect(true, NULL, $username, $password)) {
	exit(1);
}
$start_time = time();
// function hasTimedout() {
//     global $start_time;
//    return (time() - $start_time > 10);
// }
$topics['zunroof/rms/#'] = array("qos" => 0, "function" => "procmsg");
$mqtt->subscribe($topics, 0);
while ($mqtt->proc()) { }
// if(!$done){
//     $mongoConn = MyMongoDB::connect();
//     $dateTable = new DateTime();
//     $resultDate=$dateTable->format('Y-m-d');
//     $collections = $mongoConn->$deviceId->$resultDate;
//     $outparam->power = $returnArray['p1'];
//     $outparam->time = date("H").":".date("i").":".date("s");
//     $collections->insertOne($outparam);
// }
$mqtt->close();
function procmsg($topic, $msg){
    $num = date('i');
    $val = (int)$num;
    if($val%3==0){
    if(!empty($msg)){
    $returnArray = array();
    $res_temp = explode(',', $msg); 
    foreach ($res_temp as $match) { // Check through each match.
        $results = explode('=', $match); // Separate the string into key and value by '=' as delimiter.
        $returnArray[$results[0]] = trim($results[1]); // Load key and value into array.
    }
   // global $done;
    error_log(print_r($returnArray,true));
    $mongoConn = MyMongoDB::connect();
    $nthDay = date('z');
    $hour = date("H");
    $min = date("i");
    $second = date("s");
    $finalHour = $hour+($min/60)+($second/3600);
    $sunrise = Helper::getSunRise($nthDay);
    $sunset = Helper::getSunSet($nthDay);  
    if($finalHour >=$sunrise-0.1 && $finalHour<=$sunset+0.5){
try {     
    $dateTable = new DateTime();
    $resultDate=$dateTable->format('Y-m-d');
if(isset($returnArray['chipId'])){
        $dbName = $returnArray['chipId'];     
        $collections = $mongoConn->$dbName->$resultDate;
        if(isset($returnArray['p1'])){
        $outparam->power = $returnArray['p1'];
        }
        if(isset($returnArray['e1'])){
        $outparam->energy = $returnArray['e1'];
        }
        $outparam->time = date("H").":".date("i").":".date("s");
        $collections->insertOne($outparam);
        if(isset($returnArray['e1'])){
        if(date('H/i')=="13/25" || date('H/i')=="10/25" || date('H/i')=="18/25"){
            $collectMonthlyEnergy = $mongoConn->$dbName->energy_monthly;
        // $mOut->energyTotal = $multiplicationFactor*$returnArray['e1'];
        // $mOut->month = date('M-Y');
        // $collectMonthlyEnergy->insertOne($mOut);
            $collectMonthlyEnergy->updateOne(['month' => date('M-Y')],['$set' => ['energyTotal' => $returnArray['e1']]], ['upsert' => true]);
        }
        if(date('H/i')=="13/25" || date('H/i')=="10/25" || date('H/i')=="18/25"){
            $collectYearlyEnergy = $mongoConn->$dbName->energy_yearly;
        // $yOut->energyTotal = $multiplicationFactor*$returnArray['e1'];
        // $yOut->year = date('Y');
        //   $collectYearlyEnergy->insertOne($yOut);
            $collectYearlyEnergy->updateOne(['year' => date('Y')],['$set' => ['energyTotal' => $returnArray['e1']]], ['upsert' => true]);
        } 
        $collectionEnergy = $mongoConn->$dbName->energy;
        // $_SESSION[$resultDate.$dbName]=true;
        // $out->energy_total = $multiplicationFactor*$returnArray['e1'];
        // $out->date = $resultDate;
        // $collectionEnergy->insertOne($out);
    if($returnArray['e1']>0){
        $collectionEnergy->updateOne(['date' => $resultDate],['$set' => ['energy_total' => $returnArray['e1']]], ['upsert' => true]);
        }
    }
  } 
}
catch (MongoDB\Driver\Exception\InvalidArgumentException $e) {

    echo "Exception:", $e->getMessage(), "\n";
}
catch (MongoDB\Driver\Exception\UnexpectedValueException $e) {

    echo "Exception:", $e->getMessage(), "\n";
}
catch (MongoDB\Driver\Exception\BulkWriteException $e) {

    echo "Exception:", $e->getMessage(), "\n";
}
catch (MongoDB\Driver\Exception\ServerException $e) {

    echo "Exception:", $e->getMessage(), "\n";
} 
 catch (MongoDB\Driver\Exception\AuthenticationException $e) {

    echo "Exception:", $e->getMessage(), "\n";
} catch (MongoDB\Driver\Exception\ConnectionException $e) {

    echo "Exception:", $e->getMessage(), "\n";
} catch (MongoDB\Driver\Exception\ConnectionTimeoutException $e) {

    echo "Exception:", $e->getMessage(), "\n";
} catch (MongoDB\Driver\Exception\Exception $e) {

    echo "Exception:", $e->getMessage(), "\n";
}
      } 
    }
  }
}