<?php
session_start();
require 'vendor/autoload.php';
require './mongoDbConnection.php';
require '../auth/src/authentication/validUser.php';
$data_back = json_decode(file_get_contents('php://input'));
error_log(print_r($data_back,true));
$jwt = $data_back->jwt;
$userId = $data_back->userId;
$deviceId = $data_back->deviceId;
$type = $data_back->type;
error_log(print_r($type,true));
$valid = 0;
if(!isset($_SESSION['valid-token'])){
$valid = validateUser::validLogin($jwt,$userId);
}
if($valid || $_SESSION['valid-token']==true){
    $mongoconn = MyMongoDB::connect();
    $collection = null;
    if($type==1 || $type==4){
    $collection = $mongoconn->$deviceId->energy;
    } else if($type==2){
    $collection = $mongoconn->$deviceId->energy_monthly;
    } else if($type==3){
    $collection = $mongoconn->$deviceId->energy_yearly;
    }
    $_SESSION['valid-token']=true;
    $cursor = $collection->find();
    $results = array();
    foreach ( $cursor as $id => $value )
    {
       $json = MongoDB\BSON\toJSON(MongoDB\BSON\fromPHP($value));
       //error_log(print_r($json,true));
       $json =  json_decode($json, true);
       if($type==1 || $type==4){
          $val = array("date"=>$json['date'],"energy_total"=>$json['energy_total']);
         }
        else if($type==2){
          $val = array("month"=>$json['month'],"energyTotal"=>$json['energyTotal']);
         }
       else if($type==3){
          $val = array("year"=>$json['year'],"energyTotal"=>$json['energyTotal']);
        }
        array_push($results,$val);
    }
    echo json_encode($results);
}
?>
