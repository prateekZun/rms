<?php
require 'vendor/autoload.php';
session_start();
require './mongoDbConnection.php';
require '../auth/src/authentication/dbConnect.php';
require './phpMQTT.php';
require './helper/getSunriseAndSunset.php';
$server = "www.zunpulse.com";     // change if necessary
$port = 8883;                     // change if necessary
$username = "nodemcu1";                   // set your username
$password = "nodemcu1password";                   // set your password
$client_id = "publish13349"; // make sure this is unique for connecting to sever - you could use uniqid()
$multiplicationFactor = $_REQUEST['multiFactor'];
$deviceId = $_REQUEST['chipId'];
$mqtt = new phpMQTT($server, $port, $client_id,null);
if(!$mqtt->connect(true, NULL, $username, $password)) {
	exit(1);
}
$start_time = time();
function hasTimedout() {
    global $start_time;
   return (time() - $start_time > 60);
}
$topics[$deviceId] = array("qos" => 0, "function" => "procmsg");
$mqtt->subscribe($topics, 0);
while ($mqtt->proc() && !$done && !hasTimedout()) { }
if(!$done){
    $mongoConn = MyMongoDB::connect();
    $dateTable = new DateTime();
    $resultDate=$dateTable->format('Y-m-d');
    $collections = $mongoConn->$deviceId->$resultDate;
    $outparam->power = $multiplicationFactor*$returnArray['p1'];
    $outparam->time = date("H").":".date("i").":".date("s");
    $collections->insertOne($outparam);
}
$mqtt->close();
function procmsg($topic, $msg){
    if(!empty($msg)){
    $returnArray = array();
    $res_temp = explode(',', $msg); 
    foreach ($res_temp as $match) { // Check through each match.
        $results = explode('=', $match); // Separate the string into key and value by '=' as delimiter.
        $returnArray[$results[0]] = trim($results[1]); // Load key and value into array.
    }
    global $done;
    global $multiplicationFactor;
    error_log(print_r($returnArray,true));
    $mongoConn = MyMongoDB::connect();
    // $nthDay = date('z');
    // $hour = date("H");
    // $min = date("i");
    // $second = date("s");
    // $finalHour = $hour+($min/60)+($second/3600);
    // $sunrise = Helper::getSunRise($nthDay);
    // $sunset = Helper::getSunSet($nthDay);  
    $dateTable = new DateTime();
    $resultDate=$dateTable->format('Y-m-d');
    $dbName = $returnArray['chipId'];    
$collections = $mongoConn->$dbName->$resultDate;
$outparam->power = $multiplicationFactor*$returnArray['p1'];
$outparam->energy = $multiplicationFactor*$returnArray['e1'];
$outparam->time = date("H").":".date("i").":".date("s");
$collections->insertOne($outparam);
// $energyOutput->energy = $multiplicationFactor*$returnArray['e1'];
// $energyOutput->day = $resultDate;
// $document = "energy_".$resultDate;
// $dailyEnergy = $mongoConn->$dbName->$document;
// $cursor = $dailyEnergy->find();
// if($cursor->count() > 0){
//     error_log(print_r($cursor,true));
//     $criteria = array("day"=>$resultDate);
//         $newdata = array('$set'=>array("energy"=>$multiplicationFactor*$returnArray['e1']));
//         $options = array("upsert"=>true,"multiple"=>true);
 
//         $ret = $dailyEnergy->update(
//             $criteria,
//             $newdata,
//             $options
//         );
//         var_dump($ret);  
//  } else {
//     error_log(print_r("first ENtry",true)); 
//     $dailyEnergy->insertOne($energyOutput);    
// }

// } else if( $finalHour > $sunset+0.1 && !isset($_SESSION[$resultDate.$dbName])) {

    if(date('H/i')=="13/25" || date('H/i')=="10/25" || date('H/i')=="18/25"){
        $collectMonthlyEnergy = $mongoConn->$dbName->energy_monthly;
    // $mOut->energyTotal = $multiplicationFactor*$returnArray['e1'];
    // $mOut->month = date('M-Y');
    // $collectMonthlyEnergy->insertOne($mOut);
        $collectMonthlyEnergy->updateOne(['month' => date('M-Y')],['$set' => ['energyTotal' => $multiplicationFactor*$returnArray['e1']]], ['upsert' => true]);
     }
     if(date('H/i')=="13/25" || date('H/i')=="10/25" || date('H/i')=="18/25"){
        $collectYearlyEnergy = $mongoConn->$dbName->energy_yearly;
       // $yOut->energyTotal = $multiplicationFactor*$returnArray['e1'];
       // $yOut->year = date('Y');
     //   $collectYearlyEnergy->insertOne($yOut);
        $collectYearlyEnergy->updateOne(['year' => date('Y')],['$set' => ['energyTotal' => $multiplicationFactor*$returnArray['e1']]], ['upsert' => true]);
     } 
    $collectionEnergy = $mongoConn->$dbName->energy;
   // $_SESSION[$resultDate.$dbName]=true;
   // $out->energy_total = $multiplicationFactor*$returnArray['e1'];
   // $out->date = $resultDate;
   // $collectionEnergy->insertOne($out);
   if($returnArray['e1']>0){
    $collectionEnergy->updateOne(['date' => $resultDate],['$set' => ['energy_total' => $multiplicationFactor*$returnArray['e1']]], ['upsert' => true]);
   }
    $done=1;
    } else {
        $done=1;
    }
}
?>
