<?php
require 'vendor/autoload.php';
require '../auth/src/authentication/validUser.php';
require './mongoDbConnection.php';
$data_back = json_decode(file_get_contents('php://input'));
$jwt = $data_back->jwt;
$userId = $data_back->userId;
$city = $data_back->city;
$valid = 0;
$valid = validateUser::validLogin($jwt,$userId);
if($valid){
$url="http://api.openweathermap.org/data/2.5/weather?q=".$city."&units=metric&APPID=dd7f04bea8f2acc1ffb8a6722c084de6";
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
$mongoConn = MyMongoDB::connect();
$dateTable = new DateTime();
$resultDate=$dateTable->format('Y-m-d');
$collections = $mongoConn->weatherData->$resultDate;
if($collections->count()==0){
$collections->insertOne(json_decode($result,TRUE));
}
// Closing
curl_close($ch);
echo $result;
}
?>
