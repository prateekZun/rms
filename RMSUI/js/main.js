function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
               c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
      }
      return "";
    }
     function getRealtimeData(){
      var deviceId=document.getElementById("selectedDevice").value;
      var userId = getCookie("userId");
      var jwt = getCookie("jwt");
      var jsonRequest= {
                  "deviceId":deviceId,
                  "jwt":jwt,
                  "userId":userId
              };
            var data = JSON.stringify(jsonRequest);  
            var request= new XMLHttpRequest();  
            var type = document.getElementById('type').value;
            if(type=="Nise"){
            request.open("POST", "../sensor/getCurrentDataNise.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            }
            if(type=="meter"){
            request.open("POST", "../sensor/getCurrentData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            }
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200){
              var jsonResponse = JSON.parse(request.responseText);
              if(jsonResponse.error==0){
               // document.getElementById( 'dataset' ).style.display = 'block';
                document.getElementById( 'datadefault' ).style.display = 'block';
                document.getElementById( 'datadefault1' ).innerHTML = "Currently Sensor is not sending the data.We are working on it.";
                document.getElementById("total_E").innerHTML = "Data Unavailable";
                document.getElementById("kwhVal").value = "Data Unavailable";
                document.getElementById("current_i").innerHTML = "Data Unavailable";
                // document.getElementById("power_factor").innerHTML = "Data Unavailable";
                document.getElementById("power").innerHTML = "Data Unavailable";
                // document.getElementById("Frequency").innerHTML = "Data Unavailable";
                document.getElementById("Voltage_v").innerHTML = "Data Unavailable";
                // document.getElementById("humidity").innerHTML ="Data Unavailable";

            //   document.getElementById("overall_temperature").innerHTML = "Data Unavailable";

            //   document.getElementById("inside_temperature").innerHTML = "Data Unavailable";

            //   document.getElementById("outside_temperature").innerHTML = "Data Unavailable";
              } else if(type=="meter") {
                document.getElementById("powerBlock").style.display='block';
                document.getElementById("current_iBlock").style.display='block';
                document.getElementById("total_EBlock").style.display='block';
                document.getElementById("Voltage_vBlock").style.display='block';
                document.getElementById( 'dataset' ).style.display = 'block';
                document.getElementById( 'datadefault' ).style.display = 'none';
               var factor = document.getElementById("multiFactor").value;
               if(factor==1 || factor=="1"){
                var energy=(1.0*parseFloat(jsonResponse.e1)/1000.0).toFixed(2);
                var power=(1.0*parseFloat(jsonResponse.p1)/1000.0).toFixed(2);
                var current=(1.0*parseFloat(jsonResponse.i1)).toFixed(2);
               } else if(factor==3 || factor=="3"){
                var energy=(3.0*parseFloat(jsonResponse.e1)/1000.0).toFixed(2);
                var power=(3.0*parseFloat(jsonResponse.p1)/1000.0).toFixed(2);
                var current=(3.0*parseFloat(jsonResponse.i1)).toFixed(2);
               }
              document.getElementById("total_E").innerHTML = ""+energy +" kwh";
              document.getElementById("kwhVal").value = energy;
              document.getElementById("current_i").innerHTML = ""+ current +" A";
           //   document.getElementById("power_factor").innerHTML = jsonResponse.PF;
              document.getElementById("power").innerHTML = ""+ power +" kw";
           //   document.getElementById("Frequency").innerHTML = jsonResponse.f_hz+" hz";
              document.getElementById("Voltage_v").innerHTML = jsonResponse.v1 +" V";  
        
           //   document.getElementById("today_E").innerHTML = jsonResponse.Etoday+" kwhr";
              } else if (type=="Nise"){
             document.getElementById( 'otherDetails' ).style.display = 'none'; 
              var energy = 0; 
              var current = 0;
              var power =0; 
              var voltage=0;
              document.getElementById('dataset').style.display = 'block';
              document.getElementById('datadefault').style.display = 'none';
              document.getElementById("power_factorBlock").style.display='none';
              document.getElementById("FrequencyBlock").style.display='none';
              if(jsonResponse.e1!=undefined ){
                energy+=Number(jsonResponse.e1);
                document.getElementById("kwhVal").value = energy;  
              document.getElementById("inside_temperatureBlock").style.display='none';
              document.getElementById("outside_temperatureBlock").style.display='none';
              document.getElementById("overall_temperatureBlock").style.display='none';
              document.getElementById("humidityBlock").style.display='none';

              document.getElementById("powerBlock").style.display='block';
              document.getElementById("current_iBlock").style.display='block';
              document.getElementById("total_EBlock").style.display='block';
              document.getElementById("Voltage_vBlock").style.display='block';
              }
              if(jsonResponse.e2!=undefined ){
                energy+=Number(jsonResponse.e2);
                document.getElementById("kwhVal").value = energy;
              }
              if(jsonResponse.e3!=undefined ){
                energy+=Number(jsonResponse.e3);
                document.getElementById("kwhVal").value = energy;
              }
              document.getElementById("total_E").innerHTML = energy+" wh";
              if(jsonResponse.i1!=undefined ){
                current+=Math.round(Number(jsonResponse.i1));
              }
              if(jsonResponse.i2!=undefined ){
                current+=Math.round(Number(jsonResponse.i2));
              }
              if(jsonResponse.i3!=undefined ){
                current+=Math.round(Number(jsonResponse.i3));
              }
              document.getElementById("current_i").innerHTML = current+" A";
              if(jsonResponse.p1!=undefined ){
                power+=Math.round(Number(jsonResponse.p1));
              }
              if(jsonResponse.p2!=undefined ){
                power+=Math.round(Number(jsonResponse.p2));
              }
              if(jsonResponse.p3!=undefined ){
                power+=Math.round(Number(jsonResponse.p3));
              }
              document.getElementById("power").innerHTML = power+" w";
              if(jsonResponse.v1!=undefined ){
                voltage += Math.round(Number(jsonResponse.v1));
              }
              if(jsonResponse.v2!=undefined ){
                voltage = Math.round((Number(jsonResponse.v1)+Number(jsonResponse.v2))/2);
              }
              if(jsonResponse.v3!=undefined ){
                voltage = Math.round(Number((Number(jsonResponse.v1)+Number(jsonResponse.v2)+Number(jsonResponse.v3))/3));
              } 
              document.getElementById("Voltage_v").innerHTML = voltage+" V"; 
              if(jsonResponse.t0!=undefined){
              document.getElementById("inside_temperature").innerHTML = jsonResponse.t0+"degree";
              document.getElementById("powerBlock").style.display='none';
              document.getElementById("current_iBlock").style.display='none';
              document.getElementById("total_EBlock").style.display='none';
              document.getElementById("Voltage_vBlock").style.display='none';
              document.getElementById("inside_temperatureBlock").style.display='block';
              document.getElementById("outside_temperatureBlock").style.display='block';
              document.getElementById("overall_temperatureBlock").style.display='block';
              document.getElementById("humidityBlock").style.display='block';
              }
              if(jsonResponse.t1!=undefined){
              document.getElementById("outside_temperature").innerHTML = jsonResponse.t1+"degree";
              }
              if(jsonResponse.t2!=undefined){
              document.getElementById("overall_temperature").innerHTML = jsonResponse.t2+"degree";
              }
              if(jsonResponse.h0!=undefined){
              document.getElementById("humidity").innerHTML = jsonResponse.h0;
              }
              }
              }
            }      
     }
     function getWeather(){
      var city = document.getElementById("city").value;
      document.getElementById("cityName").innerHTML = city;
      var userId = getCookie("userId");
      var jwt = getCookie("jwt");
      var jsonRequest= {
                  "city":city,
                  "jwt":jwt,
                  "userId":userId
              };
            var data = JSON.stringify(jsonRequest);  
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getWeatherReport.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200){
            //  var jsonResponse = JSON.parse(request.responseText);
            var jsonOut = JSON && JSON.parse(request.responseText) || $.parseJSON(request.responseText);
              document.getElementById("weather").innerHTML = jsonOut.weather[0].description;
              document.getElementById("visibility").innerHTML = jsonOut.visibility;
              document.getElementById("clouds").innerHTML = jsonOut.clouds.all;
              document.getElementById("windspeed").innerHTML = jsonOut.wind.speed;
              document.getElementById("windDirection").innerHTML = jsonOut.wind.deg;
              document.getElementById("temp").innerHTML = jsonOut.main.temp;
              document.getElementById("pressure").innerHTML = jsonOut.main.pressure;
              document.getElementById("humidity1").innerHTML = jsonOut.main.humidity;
              document.getElementById("tempRange").innerHTML = jsonOut.main.temp_min+" - "+jsonOut.main.temp_max;
              // var sunset = toReadableString(jsonOut.main.sunset);
              // var sunrise = toReadableString(jsonOut.main.sunrise);
               var sunset = new Date(1000*jsonOut.sys.sunset);
               var sunrise = new Date(1000*jsonOut.sys.sunrise);
              // sunset = sunset.toLocaleString();
              // sunrise = sunrise.toLocaleString();
              document.getElementById("sunrise").innerHTML = sunrise.toLocaleString();
              document.getElementById("sunset").innerHTML = sunset.toLocaleString();
              }
            }      
     }

      function addDevice(){
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var deviceName = document.getElementById("devicename").value;
        var deviceId = document.getElementById("deviceid").value;  
        var devicePwd = document.getElementById("devicepwd").value;    
        var jsonRequest= {
                  "deviceName":deviceName,
                  "deviceId":deviceId,
                  "devicePassword":devicePwd,
                  "jwt":jwt,
                  "userId":userId
              };
            var data = JSON.stringify(jsonRequest);  
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/addDevice.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200) {
              var jsonResponse = JSON.parse(request.responseText);
                 alert(jsonResponse.message);
                 window.location.reload();
                }
            }
      }
  
      function viewDeviceData(deviceId,deviceName,city,type,sensorType,multifactor){
        document.getElementById("labelpe").innerHTML = "Power";  
        document.getElementById( 'loader' ).style.display = 'block';  
        document.getElementById( 'myAreaChart-power' ).style.display = 'none';
        document.getElementById( 'myAreaChart-energy' ).style.display = 'none';
        document.getElementById( 'myAreaChartTemp' ).style.display = 'none';
        if(multifactor!=null){
            document.getElementById("multiFactor").value = multifactor;
        }
        if(sensorType==null){
          sensorType = document.getElementById("graphType").value;
        } else {
          document.getElementById("graphType").value = sensorType;
        }
        if(sensorType=="temperature"){
          document.getElementById( 'temperatureGraph' ).style.display = 'block';
          document.getElementById( 'powerGraph' ).style.display = 'none';
        } else if(sensorType=="energy" || sensorType=="RMS") {
          document.getElementById( 'temperatureGraph' ).style.display = 'none';
          document.getElementById( 'powerGraph' ).style.display = 'block';
        }
        document.getElementById("selectdate").disabled = false;
        if(deviceName!=null){
          document.getElementById("selectedDeviceName").value=deviceName;
        } else {
          deviceName = document.getElementById("selectedDeviceName").value;
        }
        document.getElementById("deviceSelect").innerHTML =deviceName;
        if(type!=null){
          document.getElementById('type').value=type;
        }
        if(city!=null){
        document.getElementById("city").value=city;
        }
        var device = deviceId;
        if(device==null){
          device = document.getElementById("selectedDevice").value;
          if(device == null || device=="null"){
             alert("please choose your device");
          }
        } else {
        document.getElementById("selectedDevice").value=deviceId;
        }
       // console.log(document.getElementById("selectedDevice").value);
        var currentDate  = document.getElementById("selectdate").value;
        var splitsdate = currentDate.split('/');
        console.log(currentDate);
        var finalDate = splitsdate[2]+"-"+splitsdate[1]+"-"+splitsdate[0];
        console.log(finalDate);
        document.getElementById("displayDevice").innerHTML=document.getElementById("deviceSelect").innerHTML+"/Power";
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var jsonRequest= {
                  "deviceId":device,
                  "jwt":jwt,
                  "userId":userId,
                  "date":finalDate
              };
            var data = JSON.stringify(jsonRequest);   
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getSensorData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200) { 
              var jsonResponse = JSON.parse(request.responseText);
              console.log(jsonResponse);
              getRealtimeData();
              document.getElementById('loader').style.display = 'none';
              jsonResponse.sort(function(a, b) {
                return convertSecond(a.time)-convertSecond(b.time);
            });
        
        // -- Area Chart Impl
        if(sensorType=="energy" || sensorType=="RMS"){
            var powers = [];
            var times = [];  
            for(var i in jsonResponse) {
             if(jsonResponse[i].power!=undefined){ 
               powers.push(parseFloat(jsonResponse[i].power)/1000.0);
               times.push(jsonResponse[i].time);
             }
            }       
       // document.getElementById( 'myAreaChart-power' ).style.display = 'block';
        $('#myAreaChart-power').remove(); // this is my <canvas> element
        $('#cardbo1').append('<div id="myAreaChart-power" width="100%" height="50"><div>');
       // var ctxpower = document.getElementById("myAreaChart-power"); 

       Highcharts.chart('myAreaChart-power', {
        chart: {
            zoomType: 'x',
            spacingBottom: 30
        },
        title: {
            text: 'Power'
        },
        subtitle: {
            text: 'Unit taken as kilowatt',
            floating: true,
            align: 'right',
            verticalAlign: 'bottom',
            y: 15
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            layout: 'vertical',
            x: 0,
            y: 0,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories:times,
            tickLength:4,
            tickInterval:20
        },
        yAxis: {
            title: {
                text: 'Kw'
            },
            labels: {
                formatter: function () {
                    return Highcharts.numberFormat(this.value,2);
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.x + ': ' +Highcharts.numberFormat(this.y, 2);
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            },
            series: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'areaspline',
            name: 'POWER',
            data: powers,
            color:'#228B22'
        }]
    });
          
    } 
//     else if(sensorType=="temperature"){
//               var insideTemp = [];
//               var times = [];
//               var outsideTemp = [];
//               var temp = [];
//               var humidity = [];
//               var a =null;
//               var seconds=null;
//         for(var i in jsonResponse) {
//           if(jsonResponse[i].temperature!=undefined && jsonResponse[i].temperature!=-127.0 && jsonResponse[i].temperature!="nan"){
//             temp.push(jsonResponse[i].temperature);
//            }
//            if(jsonResponse[i].outside_temperature!="nan"){
//             outsideTemp.push(jsonResponse[i].outside_temperature); 
//            }  
//              if(jsonResponse[i].inside_temperature!=-127 && jsonResponse[i].inside_temperature!="nan"){
//               insideTemp.push(jsonResponse[i].inside_temperature);
//              }
//             //  temp.push(jsonResponse[i].temperature);
//             if(jsonResponse[i].humidity!="nan"){
//               humidity.push(jsonResponse[i].humidity);
//             }
//             times.push(jsonResponse[i].time);
//         }
//         for(var i=1;i<outsideTemp.length;i++){
//           if(outsideTemp[i]==-127 && outsideTemp[i-1]!=-127){
//             outsideTemp[i]=outsideTemp[i-1];
//           }
//         }
// // minutes are worth 60 seconds. Hours are worth 60 minutes.
//         $('#myAreaChartTemp').remove(); // this is my <canvas> element
//         $('#cardbo2').append('<canvas id="myAreaChartTemp" width="100%" height="60"><canvas>');
//         var canvas = document.getElementById("myAreaChartTemp");
//                 }
              }
            }
          }
       function getDayMonthYearView(type){
        document.getElementById("labelpe").innerHTML = "Energy/day";
        document.getElementById( 'loader' ).style.display = 'block';   
        document.getElementById( 'myAreaChart-power' ).style.display = 'none';
        document.getElementById( 'myAreaChart-energy' ).style.display = 'none'; 
        var deviceId = document.getElementById("selectedDevice").value;
        document.getElementById("selectdate").disabled = true;
        document.getElementById("displayDevice").innerHTML=document.getElementById("deviceSelect").innerHTML+"/Energy";
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var jsonRequest= {
                  "deviceId":deviceId,
                  "jwt":jwt,
                  "userId":userId,
                  "type":type
              };
            var data = JSON.stringify(jsonRequest);   
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getEnergyData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status === 200) {
            var jsonResponse = JSON.parse(request.responseText);
            console.log(jsonResponse);
            document.getElementById( 'loader' ).style.display = 'none';
        // --Bar Chart Impl
        var energies = [];
        var dates = [];
        if(type==1){   
        for(var i in jsonResponse) {   
          if(jsonResponse[i].energy_total!=undefined && jsonResponse[i].date!=undefined && jsonResponse[i].energy_total>=0){ 
          if(i>0 && jsonResponse[i].date==jsonResponse[i-1].date){
              if(jsonResponse[i].energy_total<=jsonResponse[i-1].energy_total){
              i++;
              } else {
                console.log(jsonResponse[i].energy_total+"-->"+jsonResponse[i].date); 
                if(dates[dates.length-1]==jsonResponse[i].date){
                energies[energies.length-1]=parseFloat(jsonResponse[i].energy_total/1000.0);
                dates[dates.length-1]=jsonResponse[i].date;
                } else {
                    energies.push(parseFloat(jsonResponse[i].energy_total)/1000.0);
                    dates.push(jsonResponse[i].date); 
                }
              }
          } else {   
          energies.push(parseFloat(jsonResponse[i].energy_total)/1000.0);
          dates.push(jsonResponse[i].date);
          }
          }
          }
          console.log(dates);
          console.log(energies);
          } else if(type==2){
            for(var i in jsonResponse) { 
                if(jsonResponse[i].energyTotal!=undefined && jsonResponse[i].month!=undefined){ 
                if(i>0 && jsonResponse[i].month==jsonResponse[i-1].month){
                    i++;
                } else {  
                energies.push(parseFloat(jsonResponse[i].energyTotal)/1000.0);
                dates.push(jsonResponse[i].month);     
                }
                }
                }
          } else {
            for(var i in jsonResponse) {   
                if(jsonResponse[i].energyTotal!=undefined && jsonResponse[i].year!=undefined){ 
                if(i>0 && jsonResponse[i].year==jsonResponse[i-1].year){
                    i++;
                } else {   
                energies.push((parseFloat(jsonResponse[i].energyTotal)/1000.0));
                dates.push(jsonResponse[i].year);
                }
                }
                } 
          }
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
           if(type==1){
            if(dd<10){
                  dd='0'+dd;
              } 
              if(mm<10){
                  mm='0'+mm;
              } 
              dat = yyyy+'-'+mm+'-'+dd;
              energies.push(parseFloat(document.getElementById("kwhVal").value));
              dates.push(dat);
            if(energies.length>1){
             for(var i=energies.length-1;i>0;i--){
              energies[i]=energies[i] - energies[i-1];
             }
            }
            if(dates.length>1 && dates[dates.length-1]==dates[dates.length-2]){
                dates = dates.slice(0,dates.length-1);
                energies = energies.slice(0,energies.length-1);
            }
            var str=null;
            for(i=0;i<dates.length;i++){
                str = dates[i].split("-");
                if(Number(str[1])==mm){
                    dates = dates.slice(i,dates.length);
                    energies=energies.slice(i,energies.length);
                    break;
                } 
            }
           } 
           if(type==2){
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var d = new Date();
            dates.push(monthNames[d.getMonth()]);
            energies.push(parseFloat(document.getElementById("kwhVal").value));
            if(energies.length>1){
            for(var i=energies.length-1;i>0;i--){
              energies[i]=energies[i]-energies[i-1];
             }
            }
           }
           if(type==3){
             dates.push(yyyy);
             energies.push(parseFloat(document.getElementById("kwhVal").value));
             if(energies.length>1){
             for(var i=energies.length-1;i>0;i--){
              energies[i]=energies[i]-energies[i-1];
             }
            }
           }
           if(energies.length==0){
             if(type==1){
             alert("Daywise Data not available");
             return;
             }
             if(type==2){
             alert("Monthwise Data not available");
             return;
             }
             if(type==3){
             alert("yearwise Data not available");
             return;
             }
            }
           $('#myAreaChart-energy').remove(); 
           $('#cardbo1').append('<div id="myAreaChart-energy" width="100%" div="50"><canvas>'); 

    //bar chart data
    Highcharts.chart('myAreaChart-energy', {
        chart: {
            type: 'column',
            spacingBottom: 30,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 40,
                viewDistance: 25
            }
        },
        title: {
            text: 'Energy'
        },
        subtitle: {
            text: 'Unit taken as kilowatt-hour',
            floating: true,
            align: 'right',
            verticalAlign: 'bottom',
            y: 15
        },
        xAxis: {
            categories:dates,
            labels: {
                skew3d: true,
            }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            labels: {
                format: '{value:.2f}'
            },
            title: {
                text: 'Kwh',
                skew3d: true
            },
            stackLabels: {
                enabled: false,
              //  format: '{value:.2f}',
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            layout: 'vertical',
            x: 0,
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y:.2f}'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                depth: 40,
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                }
            }
        },
        series: [{
            name: 'ENERGY',
            data: energies,
            color:'#228B22'
        }]
    });
           }
         }      
       }
       function download(){
       var sensorType = document.getElementById("graphType").value;
        document.getElementById("selectdate").disabled = false;
        var device = document.getElementById("selectedDevice").value;
        var currentDate  = document.getElementById("selectdate").value;
        var splitsdate = currentDate.split('/');
        var finalDate = splitsdate[2]+"-"+splitsdate[1]+"-"+splitsdate[0];
        document.getElementById("displayDevice").innerHTML=document.getElementById("deviceSelect").innerHTML+"/Power";
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var phone = document.getElementById("phoneNumber").value;
        if(phone=="919717452684" && sensorType!="temperature"){
           device = device+"_dump";
        }
        var jsonRequest= {
                  "deviceId":device,
                  "jwt":jwt,
                  "userId":userId,
                  "date":finalDate
              };
            var data = JSON.stringify(jsonRequest);   
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getSensorData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200) { 
              var jsonResponse = JSON.parse(request.responseText);
                if(jsonResponse == '')
                    return;
        
        JSONToCSVConvertor(jsonResponse, "Report", true); 
       }
            }
       }  
 Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('');
};      
 function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[1]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 1; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var date = new Date();
    var fileName = date.yyyymmdd();
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
function convertSecond(hms){
    var a = hms.split(':'); // split it at the colons
    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    return seconds;
}
