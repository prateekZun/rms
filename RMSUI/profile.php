<?php
if(!isset($_COOKIE['userId']) || !isset($_COOKIE['jwt'])){
  header('Location:./login.php');
} else {
require '../auth/src/authentication/validUser.php';  
$validate = ValidateUser::validLogin($_COOKIE['jwt'],$_COOKIE['userId']);
$deviceList = array();
if($validate){
  $config['db']['host'] = "localhost";
  $config['db']['username']  = "root";
  $config['db']['password'] = "zunganman@MR123";
  $config['db']['database'] = "zunpulse";
  $config['jwt']['key'] = 'D809526552FBF43AB6748A69FBAF99BCABD77390F1D516D9A0E355D70DC4B576';
  $conn = MyDatabase::connect($config['db']['host'],$config['db']['database'],$config['db']['username'],$config['db']['password']);
    $query = $conn->prepare('SELECT *FROM userdevicemapping WHERE userid=:uid');
    $query->bindParam(':uid',$_COOKIE['userId']);
    $query->execute();
    if($query->rowCount()>0){
    $rslList = $query->fetchAll(PDO::FETCH_ASSOC);
    $user = $conn->prepare('SELECT *FROM users WHERE id=:usid');
    $user->bindParam(':usid',$_COOKIE['userId']);
    $user->execute();
    $detailUser=$user->fetch(PDO::FETCH_ASSOC);
    foreach($rslList as $id => $value){
      $query1 = $conn->prepare('SELECT deviceName,deviceId,installation_date,city,type,sensorType,mutiFactor FROM devicedetail where deviceId=:deviceId');
      $query1->bindParam(':deviceId',$value['deviceId']);
      if($query1->execute()){
          array_push($deviceList,$query1->fetch(PDO::FETCH_ASSOC));
      }
    }
  } else {
    $deviceList = null;
  }
} else {
  header('Location:./login.php');
  exit();
}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Zunroof-RMS</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

  <link href="styles/glDatePicker.flatwhite.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="css/new.css">

<style>

.col-sm-3{
  float: left;
  text-align: center;
  margin-top: 9px;
  margin-bottom: 9px;
}
.container-fluid{
  float: left;
  padding-left: 0px;
}
.row{
  margin-right: 0px;
  margin-left: 0px;
}
.breadcrumb{
  width: 100%;
  background: #22272a;
  border-radius: 0.25rem;
  padding:0.55rem 1rem;
}
.mb-3{
  display: inline-block;
  width: 100%;
  box-shadow: 0px 5px 15px #d4d4d4;
}
footer.sticky-footer{
	height: 40px;
	color: #212529;
	border-top: 1px solid rgba(0,0,0,0.125);
	line-height: 38px;
	text-align: center;
}
.dropdown-menu{
  background-color: #2d3439;
}
</style> 
</head>

<body class="fixed-nav sticky-footer" id="page-top">
<!-- header-->
  <nav class="navbar fixed-top" id="grad" >
    <a class="navbar-brand" href="profile.php" style="color: #22272a;"><font size="6">RMS</font>(Beta)</a>
    <div class="username">
      <div class="nav-link name"><?=$detailUser['phone']?></div>
      <input type="text" value="<?php echo $detailUser['phone'] ?>" id="phoneNumber" style="display:none">
      <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
        <i class="fa fa-fw fa-sign-out"></i>
        Logout
      </a>  
    </div>
  </nav>

<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
          <!-- <a class="nav-link">
            <i class="fa fa-fw fa-calendar-plus-o"></i>
            <span class="nav-link-text">Energy</span>
          </a>
          <a class="nav-link">
            <i class="fa fa-fw fa-plug"></i>
            <span class="nav-link-text">Power</span>
          </a>
          <a class="nav-link">
            <i class="fa fa-fw fa-tachometer"></i>
            <span class="nav-link-text">Current</span>
          </a> -->
      <ul class="navbar-nav" id="others">
        <!-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-th-large"></i>
            <span class="d-lg">Others
              <span class="badge badge-pill badge-primary"></span>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header link1">Temperature</h6>
            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header link1">Humidity</h6>
          </div>
        </li> -->
      </ul>  
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-object-group"></i>
            <span class="d-lg" id="deviceSelect">Select Device
              <span class="badge badge-pill badge-primary"></span>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <?php foreach($deviceList as $key => $val){
              echo '<a href="javascript:void(0)" class="dropdown-item" 
              onclick="viewDeviceData(\''.$val["deviceId"].'\',\''.$val["deviceName"].'\',\''.$val["city"].'\',\''.$val["type"].'\',\''.$val["sensorType"].'\',\''.$val["mutiFactor"].'\')">'.$val["deviceName"].'</a>';
             ?>
             <input type="text" id="selectedDevice" value="<?php echo $deviceList[0]['deviceId'] ?>" style="display:none"></input>
             <input type="text" id="selectedDeviceName" value="<?php echo $deviceList[0]['deviceName'] ?>" style="display:none"></input>
             <div class="dropdown-divider1"></div>
             <?php } ?>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw  fa-plus-square-o"></i>
            <span class="d-lg">Add Device
              <span class="badge badge-pill badge-primary"></span>
            </span>
          </a>
          <div class="dropdown-menu addblock" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">Add Device:</h6>
            <div class="dropdown-divider"></div>
            <input class="form-control adddevice " id="devicename" type="text" placeholder="Enter Device Name">
            <input class="form-control adddevice " id="deviceid" type="text" placeholder="Enter Device ID">
            <input class="form-control adddevice " id="devicepwd" type="password" placeholder="Enter Password">
            <a class="btn btn-primary btn-block adddevicebutton" onclick="addDevice()">Add Device</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>


<div class="content-wrapper" style="margin-top: 40px;">
<div class="row col-sm-12 part1">
<div class="card mb-3" id="datadefault" style="display:block">
<center>
 <span id="datadefault1" style="color:green; font-size: 30px;">Please Select Device to view Sensor Data</span>
 </center>
</div>
<div class="card mb-3" id="dataset" style="display:none">
<div class="col-sm-3 block" id="current_iBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100" style="background-color:#228B22 !important">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="current_i">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Current
    </div>
  </div>
</div>


<div class="col-sm-3 block" id="Voltage_vBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100" style="background-color:#228B22 !important">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small"id="Voltage_v">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Voltage
    </div>
  </div>
</div>

<div class="col-sm-3 block" id="total_EBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100" style="background-color:#228B22 !important">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="total_E">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Total Energy
    </div>
  </div>
</div>

<!-- <div class="col-sm-3 block" >
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="today_E"></div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Today Energy
    </div>
  </div>
</div> -->

<div class="col-sm-3 block" id="powerBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100" style="background-color:#228B22 !important">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="power">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Power
    </div>
  </div>
</div>

<!-- <div class="col-sm-3 block" id="power_factorBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="power_factor">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Power Factor
    </div>
  </div>
</div> -->

<!-- <div class="col-sm-3 block" id="FrequencyBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="Frequency">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Frequency
    </div>
  </div>
</div> -->

<!-- <div class="col-sm-3 block" id="humidityBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="humidity">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Humidity
    </div>
  </div>
</div> -->

<!-- <div class="col-sm-3 block" id="inside_temperatureBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="inside_temperature">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Inside Temperature
    </div>
  </div>
</div>

<div class="col-sm-3 block" id="outside_temperatureBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="outside_temperature">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
        Outside Temperature
    </div>
  </div>
</div>

<div class="col-sm-3 block" id="overall_temperatureBlock" style="display:none">
    <div class="card text-white bg-primary o-hidden h-100">
      <div class="card-body">
        <div class="card-body-icon">
          <i class="fa fa-fw fa-calendar-plus-o"></i>
        </div>
      <div class="small" id="overall_temperature">Select Device</div>
    </div>
    <div class="card-footer text-white clearfix z-1">
         Temperature
    </div>
  </div> -->


    <!-- <div class="col-sm-3 block" >
        <div class="card text-white bg-primary o-hidden h-100">
          <div class="card-body">
            <div class="card-body-icon">
              <i class="fa fa-fw fa-calendar-plus-o"></i>
            </div>
          <div class="small"></div>
        </div>
        <div class="card-footer text-white clearfix z-1">
            Total
        </div>
      </div>
    </div>   -->
</div>	
</div>
<div class="col-sm-12">
    <div class="container-fluid col-sm-9">
    <!-- Breadcrumbs-->
      	<ol class="breadcrumb">
      		<div class="col-sm-12 blockpad">
      			<div class="col-sm-3 dname">
        			<li class="breadcrumb-item">
          				<a href="javascript:void(0)" id="displayDevice"></a>
        			</li>        			
  				</div>    
      		<div class="col-sm-9 Sdate" style="color: #fff">
      			<input type="text" readonly="true" id="selectdate" placeholder="Select Date" />
            <div class="monthwise">
       <a href="javascript:void(0)" onclick="viewDeviceData(null,null,null,null,null,null)"class="Sday">Time</a>
              <a href="javascript:void(0)" onclick="getDayMonthYearView(1)" class="Sday">Day</a>
              <a href="javascript:void(0)" onclick="getDayMonthYearView(2)" class="Sday">Month</a>
              <a href="javascript:void(0)" onclick="getDayMonthYearView(3)" class="Sday">Year</a>
        		</div>
      	  </div>
        </ol>

      <!-- Area Chart Example-->
      <div class="card mb-3" id="powerGraph" style="display:none">
        <input type="text" id="graphType" value="<?php echo $deviceList[0]['sensorType'] ?>" style="display:none">
        <input type="text" id="multiFactor" value="<?php echo $deviceList[0]['mutiFactor'] ?>" style="display:none">
        <div class="card-header">
          <i class="fa fa-area-chart" id="labelpe">Power</i>
          <button class='download' style="float:right" onclick="download()">Download</button>
          </div>
          <div class="card-body" id="cardbo1">
          <center>
          <div class="loader" id="loader">
          </div>
          </center>
            <div id="myAreaChart-power" width="100%" height="60"></div>
            <div id="myAreaChart-energy" style="display:none" width="100%" height="60"></div>
          </div>
        <style>

      /* Rotation */
@keyframes rotate {
  from {transform: rotate(0deg);}
  to {transform: rotate(360deg);}
}

/* Square */
#loader, 
#loader:before,
#loader:after {
width: 4em;
height: 4em;
content: "";
background-color: #228B22; 
}

/* Position */
#loader {
margin: 4em auto;
position: relative;
}

/* Animate */
#loader {
animation: rotate 2.5s infinite linear;
}

/* Position */
#loader:before,
#loader:after {
position: absolute;
left: 0;
top: 0;
}

/* Star shape */
#loader:before {
 transform: rotate(60deg);
}
#loader:after {
 transform: rotate(-60deg);
}

        </style>  
      </div>
      <div class="card mb-3" id="temperatureGraph">
        <div class="card-header">
          <i class="fa fa-area-chart"></i>
          <button class='download' style="float:right"></button>
          </div>
          </style>
        <div class="card-body" id="cardbo2"> 
          <div id="myAreaChartTemp" width="100%" height="60"></div>
        </div>
      </div>

      <div class="row">
          <!-- Example Bar Chart Card-->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fa fa-bar-chart"></i></div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-8 my-auto">
                  <div id="myBarChart1" width="100" height="50"></div>
                </div>
                <div class="col-sm-4 text-center my-auto">
                  <div class="h4 mb-0 text-primary"></div>
                  <div class="small text-muted"></div>
                  <hr>
                  <div class="h4 mb-0 text-warning"></div>
                  <div class="small text-muted"></div>
                  <hr>
                  <div class="h4 mb-0 text-success"></div>
                  <div class="small text-muted"></div>
                </div>
              </div>
            </div>
          </div>
      
          <!-- Example Pie Chart Card-->
          <div class="card mb-3" style="display:none">
            <div class="card-header">
              <i class="fa fa-pie-chart"></i></div>
            <div class="card-body">
              <div id="myPieChart1" width="100%" height="50"></div>
            </div>
          </div>
        </div>
    </div>


   	<div class="col-sm-3 col2">
    	<div class="card mb-3">
        	<div class="card-header bg-warning" style="background-color:#228B22 !important">
        		<i class="fa fa-industry"></i> 
        		Plant Details
        	</div>
        	<div class="card-body">
        		<div class="col-sm-12 row1">
        			<div class="col-sm-5 data1">UserId</div>
        			<div class="col-sm-6 data2"><?=$detailUser['id']?></div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data1">Username</div>
        			<div class="col-sm-6 data2"><?=$detailUser['phone']?></div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data1">Install. Date</div>
        			<div class="col-sm-6 data2">27-Dec-2017</div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data1">Type</div>
        			<div class="col-sm-6 data2">Grid-Tied</div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data1">City</div>
        			<div class="col-sm-6 data2" id="cityName">
             
              </div>
              <input id="city" value="<?php echo $deviceList[0]['city']?>" style="display:none"></input>

        		</div>
        	</div>     
      	</div>
    </div>

    <div class="col-sm-3 col2">
    	<div class="card mb-3">
        	<div class="card-header bg-danger" style="background-color:#228B22 !important">
        		<i class="fa fa-soundcloud"></i> 
        		Weather Factors
        	</div>
        	<div class="card-body">
            <div class="col-sm-12 row1">
            	<div class="col-sm-5 data2">Weather</div>
        			<div class="col-sm-6 data1" id="weather"></div>
        		</div>
            <div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Temperature</div>
        			<div class="col-sm-6 data1" id="temp"></div>
        		</div>
            <div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Pressure</div>
        			<div class="col-sm-6 data1" id="pressure"></div>
        		</div>
            <div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Humidity</div>
        			<div class="col-sm-6 data1" id="humidity1"></div>
        		</div>
            <div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Temp Range</div>
        			<div class="col-sm-6 data1" id="tempRange"></div>
        		</div>	
            <div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Sunrise</div>
        			<div class="col-sm-6 data1" id="sunrise"></div>
        		</div>	
            <div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Sunset</div>
        			<div class="col-sm-6 data1" id="sunset"></div>
        		</div>
            <div class="col-sm-12 row1">
        			<div class="col-sm-5 data2">Visibility</div>
        			<div class="col-sm-6 data1" id="visibility"></div>
        		</div>
            <div class="col-sm-12 row1">
        			<div class="col-sm-5 data2">Clouds</div>
        			<div class="col-sm-6 data1" id="clouds"></div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Wind Speed</div>
        			<div class="col-sm-6 data1" id="windspeed"></div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Wind Direction</div>
        			<div class="col-sm-6 data1" id="windDirection"></div>
        		</div>	
        	</div>     
      	</div>
    </div>

</div>
<div class="col-sm-3 col2">
    	<div class="card mb-3" id="otherDetails">
        	<div class="card-header bg-primary" style="background-color:#228B22 !important">
        		<i class="fa fa-inr"></i> 
        		Other Revenue
        	</div>
        	<div class="card-body">
        		<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Money Saved</div>
        			<div class="col-sm-6 data1">₹ 32,268 </div>
        		</div>
        		<div class="col-sm-12 row1">
        			<div class="col-sm-5 data2">CO₂ Reduced</div>
        			<div class="col-sm-6 data1">5239 Kg</div>
        		</div>
	        	<div class="col-sm-12 row1">
    	    		<div class="col-sm-5 data2">Coal Saved</div>
        			<div class="col-sm-6 data1">826 Kg</div>
              <input type="text" id="type" value="<?php echo $deviceList[0]['type'] ?>" style="display:none"></input> 
        		</div>
        	</div>     
      	</div>
    </div>    
    <!-- /.container-fluid-->
 
    <!-- /.content-wrapper-->

    <footer class="sticky-footer">
          Copyright © ZunRoof Tech Pvt Ltd
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave ?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              <input type="text" style="display:none" id="kwhVal">
            </button>
          </div>
          <div class="modal-body">Select "LogOut" to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="../auth/src/authentication/logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="glDatePicker.min.js"></script>	
<script src="js/main.js"></script>
<script type="text/javascript">
	$(document).ready (function(){
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = (day)+"/"+(month)+"/"+now.getFullYear();
    $('#selectdate').glDatePicker();
    $('#selectdate').val(today);
    viewDeviceData(null,null,null,null,null,null);
    getWeather();
	});
</script>    
    <!-- Bootstrap core JavaScript-->
    
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <!-- <script src="vendor/chart.js/Chart.min.js"></script> -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
 
    <!-- Custom scripts for all pages-->
    <!-- <script src="js/sb-admin.min.js"></script>  
    <script src="js/sb-admin-charts.min.js"></script> -->
    <script>
      //$(document).ready(getRealtimeData); 
      // if(document.getElementById("selectedDevice").value!="null" && document.getElementById("selectedDevice").value!=null){
     // setInterval(getRealtimeData, 30000);
      // }
     
    </script> 
  </div>
</body>
</html>
