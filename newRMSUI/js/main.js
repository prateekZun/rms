function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
               c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
      }
      return "";
    }
     function getRealtimeData(){
      var deviceId=document.getElementById("selectedDevice").value;
      var userId = getCookie("userId");
      var jwt = getCookie("jwt");
      var version = document.getElementById("version").value;
      var jsonRequest= {
                  "deviceId":deviceId,
                  "jwt":jwt,
                  "userId":userId,
                  "version":version
              };
            var data = JSON.stringify(jsonRequest);  
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getCurrentData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200){
              var jsonResponse = JSON.parse(request.responseText);
              console.log(jsonResponse);
              if(jsonResponse.error==0){
               // document.getElementById("total_E").innerHTML = "Data Unavailable";
                document.getElementById("kwhVal").value = "Data Unavailable";
                document.getElementById("power").innerHTML = "Data Unavailable";
                document.getElementById("moneySaved").innerHTML ="Data Unavailable";
              } else {
                document.getElementById("powerBlock").style.display='block';
                document.getElementById("total_EBlock").style.display='block';
              
               var factor = document.getElementById("multiFactor").value;
               if(factor==1 || factor=="1"){
                var energy=(1.0*parseFloat(jsonResponse.e1)/1000.0).toFixed(2);
                var power=(1.0*parseFloat(jsonResponse.p1)/1000.0).toFixed(2);
            //    var current=(1.0*parseFloat(jsonResponse.i1)).toFixed(2);
               } else if(factor==3 || factor=="3"){
                var energy=(3.0*parseFloat(jsonResponse.e1)/1000.0).toFixed(2);
                var power=(3.0*parseFloat(jsonResponse.p1)/1000.0).toFixed(2);
            //    var current=(3.0*parseFloat(jsonResponse.i1)).toFixed(2);
               }
           //   document.getElementById("total_E").innerHTML = ""+energy +" 	kW⋅h";
              document.getElementById("kwhVal").value = energy;
              document.getElementById("power").innerHTML = ""+ power +" kW";
              var terrif =  document.getElementById("terrif").value;
              var x = (energy*parseFloat(terrif)).toFixed(2);
              var res= x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              document.getElementById("moneySaved").innerHTML = "₹ "+res;
              document.getElementById("moneyTitle").title = "assuming rate as"+terrif+"/unit";
              document.getElementById("moneySavedDn").innerHTML = "₹ "+res;
              document.getElementById("moneySavedDn").title = "assuming rate as"+terrif+"/unit";
              document.getElementById("co2Reduced").innerHTML = (0.744*x).toFixed(2) +" kg";
              document.getElementById("coalSaved").innerHTML = (0.369*x).toFixed(2) +" kg";
              } 
            }
        }      
     }
     function changePassword(){
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var oldPassword = document.getElementById("oldPassword").value; 
        var newPassword = document.getElementById("newPassword").value;
        var deviceId = document.getElementById("deviceidUp1").value;
        var jsonRequest= {
            "oldPassword":oldPassword,
            "newPassword":newPassword,
            "deviceId":deviceId,
            "jwt":jwt,
            "userId":userId
        };
        var data = JSON.stringify(jsonRequest);  
        var request= new XMLHttpRequest();  
        request.open("POST", "../sensor/changePassword.php", true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(data);
        request.onreadystatechange = function() {
          if (request.readyState == 4 && request.status === 200) {
          var jsonResponse = JSON.parse(request.responseText);
             alert(jsonResponse.message);
             window.location.reload();
            }
        }  
     }
     function deleteDevice(){
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var password = document.getElementById("delPass").value;
        var deviceId = document.getElementById("deviceidUp").value; 
        var jsonRequest= {
            "password":password,
            "deviceId":deviceId,
            "jwt":jwt,
            "userId":userId
        };
        var data = JSON.stringify(jsonRequest);  
        var request= new XMLHttpRequest();  
        request.open("POST", "../sensor/deleteDeviceMapping.php", true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(data);
        request.onreadystatechange = function() {
          if (request.readyState == 4 && request.status === 200) {
          var jsonResponse = JSON.parse(request.responseText);
             alert(jsonResponse.message);
             window.location.reload();
            }
        }  
     }
     function clearNotification(){
         var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var phone = document.getElementById("phoneNumber").value;
        var jsonRequest= {
            "phn":phone,
            "jwt":jwt,
            "userId":userId
        };
        var data = JSON.stringify(jsonRequest);  
        var request= new XMLHttpRequest();  
        request.open("POST", "../sensor/removeNotifications.php", true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(data);
        request.onreadystatechange = function() {
          if (request.readyState == 4 && request.status === 200) {
          var jsonResponse = JSON.parse(request.responseText);
             window.location.reload();
            }
        }  
     }
     function updateName(){
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var deviceId = document.getElementById("deviceidUp2").value; 
        var dName = document.getElementById("deviceN").value;
        var jsonRequest= {
            "deviceName":dName,
            "deviceId":deviceId,
            "jwt":jwt,
            "userId":userId
        };
        var data = JSON.stringify(jsonRequest);  
        var request= new XMLHttpRequest();  
        request.open("POST", "../sensor/updateDeviceName.php", true);
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.send(data);
        request.onreadystatechange = function() {
          if (request.readyState == 4 && request.status === 200) {
          var jsonResponse = JSON.parse(request.responseText);
             alert(jsonResponse.message);
             window.location.reload();
            }
        }      
     }
     function updateTerrif(){
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var deviceId = document.getElementById("deviceidUp3").value; 
        var terrif = document.getElementById("ebTerif").value;
        var jsonRequest= {
                  "terrif":terrif,
                  "deviceId":deviceId,
                  "jwt":jwt,
                  "userId":userId
              };
              var data = JSON.stringify(jsonRequest);  
              var request= new XMLHttpRequest();  
              request.open("POST", "../sensor/updateTerrif.php", true);
              request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
              request.send(data);
              request.onreadystatechange = function() {
                if (request.readyState == 4 && request.status === 200) {
                var jsonResponse = JSON.parse(request.responseText);
                   alert(jsonResponse.message);
                   window.location.reload();
                  }
              }      
     }
     function getWeather(){
      var city = document.getElementById("city").value;
      document.getElementById("cityName").innerHTML = city;
      var userId = getCookie("userId");
      var jwt = getCookie("jwt");
      var jsonRequest= {
                  "city":city,
                  "jwt":jwt,
                  "userId":userId
              };
            var data = JSON.stringify(jsonRequest);  
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getWeatherReport.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200){
            var jsonOut = JSON && JSON.parse(request.responseText) || $.parseJSON(request.responseText);
              document.getElementById("weather").innerHTML = jsonOut.weather[0].description;
              document.getElementById("visibility").innerHTML = jsonOut.visibility;
              document.getElementById("clouds").innerHTML = jsonOut.clouds.all;
              document.getElementById("windspeed").innerHTML = jsonOut.wind.speed;
              document.getElementById("windDirection").innerHTML = jsonOut.wind.deg;
              document.getElementById("temp").innerHTML = jsonOut.main.temp;
              document.getElementById("pressure").innerHTML = jsonOut.main.pressure;
              document.getElementById("humidity1").innerHTML = jsonOut.main.humidity;
               var sunset = new Date(1000*jsonOut.sys.sunset);
               var sunrise = new Date(1000*jsonOut.sys.sunrise);
              document.getElementById("sunrise").innerHTML = sunrise.toLocaleString();
              document.getElementById("sunset").innerHTML = sunset.toLocaleString();
              }
            }      
     }

      function addDevice(){
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var deviceName = document.getElementById("devicename").value;
        var deviceId = document.getElementById("deviceid").value;  
        var devicePwd = document.getElementById("devicepwd").value;    
        var jsonRequest= {
                  "deviceName":deviceName,
                  "deviceId":deviceId,
                  "devicePassword":devicePwd,
                  "jwt":jwt,
                  "userId":userId
              };
            var data = JSON.stringify(jsonRequest);  
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/addDevice.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200) {
              var jsonResponse = JSON.parse(request.responseText);
                 alert(jsonResponse.message);
                 window.location.reload();
                }
            }
      }
  
      function viewDeviceData(deviceId,deviceName,city,version,sensorType,multifactor,systemSize,terrif,installation_date,finalDt){
        // document.getElementById( 'tableIdEnergy' ).style.display = 'none';
        document.getElementById( 'tableId' ).style.display = 'block';
        $('#tableId').DataTable().destroy();
        $('#tableIdEnergy').DataTable().destroy(); 
        document.getElementById("labelpe").innerHTML = "Power";  
        document.getElementById( 'loader' ).style.display = 'block';  
        document.getElementById( 'myAreaChart-power' ).style.display = 'none';
        document.getElementById( 'myAreaChart-energy' ).style.display = 'none';
        if(multifactor!=null){
            document.getElementById("multiFactor").value = multifactor;
        }
        if(sensorType==null){
          sensorType = document.getElementById("graphType").value;
        } else {
          document.getElementById("graphType").value = sensorType;
        }
        if(sensorType=="RMS") {
          document.getElementById( 'powerGraph' ).style.display = 'block';
        }
        document.getElementById("selectdate").disabled = false;
        if(deviceName!=null){
          document.getElementById("selectedDeviceName").value=deviceName;
          document.getElementById("deviceNam").innerHTML=deviceName;
          document.getElementById("deviceN").innerHTML=deviceName;   
        } else {
          deviceName = document.getElementById("selectedDeviceName").value;
          document.getElementById("deviceNam").innerHTML = deviceName; 
          document.getElementById("deviceN").innerHTML = deviceName;
        }
        document.getElementById("deviceSelect").innerHTML ="Selected Device: "+deviceName;
        if(city!=null){
        document.getElementById("city").value=city;
        }
        if(version!=null){
            document.getElementById("version").value=version;
            document.getElementById("vers").innerHTML = version;  
        } else {
                document.getElementById("vers").innerHTML = document.getElementById("version").value;  
            }
        if(terrif!=null){
            document.getElementById("terrif").value=terrif;
        }
        if(installation_date!=null){
            document.getElementById("installation_date").value=installation_date;
            document.getElementById("installation").innerHTML = installation_date;  
        } else {
                document.getElementById("installation").innerHTML = document.getElementById("installation_date").value;  
            }
        if(systemSize!=null){
            document.getElementById("systemSize").value=systemSize;
            document.getElementById("sysSize").innerHTML = systemSize;
        } else {
            document.getElementById("sysSize").innerHTML = document.getElementById("systemSize").value;  
        }

        var device = deviceId;
        if(device==null){
          device = document.getElementById("selectedDevice").value;
          if(device == null || device=="null"){
             alert("please choose your device");
          }
            document.getElementById("deviceidUp").value = device;
            document.getElementById("deviceidUp1").value = device;
            document.getElementById("deviceidUp2").value = device;
            document.getElementById("deviceidUp3").value = device;
        } else {
            document.getElementById("selectedDevice").value=deviceId;
            document.getElementById("deviceidUp").value=deviceId;
            document.getElementById("deviceidUp1").value = device;
            document.getElementById("deviceidUp2").value = device;
            document.getElementById("deviceidUp3").value = device;
        }
        var finalDate  = document.getElementById("selectdate").value;
        if(finalDt!=null){
            finalDate = finalDt;
        }
        // var splitsdate = currentDate.split('/');
        // console.log(currentDate);
        // var finalDate = splitsdate[2]+"-"+splitsdate[0]+"-"+splitsdate[1];
        console.log(finalDate);
        document.getElementById("displayDevice").innerHTML=document.getElementById("deviceSelect").innerHTML+"/Power";
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var jsonRequest= {
                  "deviceId":device,
                  "jwt":jwt,
                  "userId":userId,
                  "date":finalDate
              };
            var data = JSON.stringify(jsonRequest);   
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getSensorData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200) { 
              var jsonResponse = JSON.parse(request.responseText);
            var factor = document.getElementById("multiFactor").value;
            if(document.getElementById("version").value!=0){
                for (var i = 0; i < jsonResponse.length; i++) {
                    if(jsonResponse[i].power!=undefined ){ 
                        jsonResponse[i].power = parseInt(factor)*parseInt(jsonResponse[i].power);
                        jsonResponse[i].energy = parseInt(factor)*parseInt(jsonResponse[i].energy);
                    }
                  }
            }
              document.getElementById('loader').style.display = 'none';
              jsonResponse.sort(function(a, b) {
                return convertSecond(a.time)-convertSecond(b.time);
            });
            // var low = 0;
            // var high = jsonResponse.length-1;
            // while(low < high){
            //     if(jsonResponse[low].energy==undefined){
            //        low++;
            //     }
            //     else if(jsonResponse[high].energy==undefined){
            //         high--;
            //     }
            //     else {
            //         todayEnergy = parseInt(factor)*(parseInt(jsonResponse[high].energy)-parseInt(jsonResponse[low].energy));
            //         break;
            //     }
            //  } 
            //  document.getElementById("total_E").innerHTML = ""+todayEnergy +" 	kW⋅h";  
        // -- Area Chart Impl
        if(sensorType=="RMS"){
            var powers = [];
            var times = [];
            var energy =[]; 
            var todayEnergy = 0; 
            for(var i in jsonResponse) {
             if(jsonResponse[i].power!=undefined){ 
               powers.push(parseFloat(jsonResponse[i].power)/1000.0);
               times.push(jsonResponse[i].time);
             }
             if(jsonResponse[i].energy!=undefined && parseFloat(jsonResponse[i].energy)>0){ 
                energy.push(parseFloat(jsonResponse[i].energy)/1000.0);
             }
            } 
            if(energy.length<2){
                todayEnergy = 0;
            } else {
            todayEnergy = (energy[energy.length-1]-energy[0]).toFixed(2);
            }
            console.log(energy);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            if(dd<10){
                  dd='0'+dd;
              } 
              if(mm<10){
                  mm='0'+mm;
              } 
              dat = yyyy+'-'+mm+'-'+dd;
              dat1= mm+'/'+dd+'/'+yyyy;
              console.log(dat1+"="+finalDate);
              console.log(finalDate==dat1);
              if(finalDate==dat1){
              document.getElementById("total_E").innerHTML = ""+todayEnergy+" 	kW⋅h"; 
              }
              document.getElementById("dateFin").innerHTML = dat; 
              document.getElementById("energyFin").innerHTML = todayEnergy; 
         //   var jsonString= JSON.stringify(obj);  
            $('#tableId').DataTable();     
        $('#myAreaChart-power').remove(); // this is my <canvas> element
        $('#powerGraph').append('<div id="myAreaChart-power" class="dashboard-flot-chart"></div>');

       Highcharts.chart('myAreaChart-power', {
        chart: {
            zoomType: 'x',
            spacingBottom: 30
        },
        title: {
            text: 'Power'
        },
        subtitle: {
            text: 'Unit taken as kilowatt',
            floating: true,
            align: 'right',
            verticalAlign: 'bottom',
            y: 15
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            layout: 'vertical',
            x: 0,
            y: 0,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories:times,
            tickLength:4,
            tickInterval:20
        },
        yAxis: {
            title: {
                text:null
            },
            labels: {
                formatter: function () {
                    return Highcharts.numberFormat(this.value,2);
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.x + ': ' +Highcharts.numberFormat(this.y, 2);
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            },
            series: {
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'areaspline',
            name: 'POWER '+finalDate,
            data: powers,
            color:'#228B22'
        }]
    }); 
    getRealtimeData();     
    } 
              }
            }
          }
       function getDayMonthYearView(type,startDate,endDate){
        document.getElementById( 'tableIdEnergy' ).style.display = 'block';
        document.getElementById( 'tableId' ).style.display = 'none';   
        $('#tableId').DataTable().destroy();
        $('#tableIdEnergy').DataTable().destroy();
        document.getElementById("labelpe").innerHTML = "Energy/day";
        document.getElementById( 'loader' ).style.display = 'block';   
        document.getElementById( 'myAreaChart-power' ).style.display = 'none';
        document.getElementById( 'myAreaChart-energy' ).style.display = 'none'; 
        var deviceId = document.getElementById("selectedDevice").value;
        document.getElementById("selectdate").disabled = true;
        document.getElementById("displayDevice").innerHTML=document.getElementById("deviceSelect").innerHTML+"/Energy";
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var jsonRequest = null;
        if(startDate==null || endDate==null){
        var jsonRequest= {
                  "deviceId":deviceId,
                  "jwt":jwt,
                  "userId":userId,
                  "type":type
              };
            } else {
                var jsonRequest= {
                    "deviceId":deviceId,
                    "jwt":jwt,
                    "userId":userId,
                    "startDate":startDate,
                    "endDate":endDate
                };   
            }
            var data = JSON.stringify(jsonRequest);   
            var request= new XMLHttpRequest();  
            if(startDate==null || endDate==null){
            request.open("POST", "../sensor/getEnergyData.php", true);
            } else {
            request.open("POST", "../sensor/getEnergyByDateRange.php", true);    
            }
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status === 200) {
            var jsonResponse = JSON.parse(request.responseText);
            var factor = document.getElementById("multiFactor").value;
            if(document.getElementById("version").value!=0){
                for (var i = 0; i < jsonResponse.length; i++) {
                    if(jsonResponse[i].energy_total!=undefined){ 
                        jsonResponse[i].energy_total = parseInt(factor)*parseInt(jsonResponse[i].energy_total);
                    }
                  }
            }
            console.log(jsonResponse);
      //      if(type==1){        
      //  } 
        // else if(type==2){
        //     $('#tableIdEnergy').DataTable( {
        //         "data" : jsonResponse,
        //         "columns" : [
        //             { "data": "month" },
        //             { "data": "energyTotal"}
        //         ]
        //     } );
        // } else {
        //     $('#tableIdEnergy').DataTable( {
        //         "data" : jsonResponse,
        //         "columns" : [
        //             { "data": "year" },
        //             { "data": "energyTotal"}
        //         ]
        //     } );
        // }
            document.getElementById( 'loader' ).style.display = 'none';
        // --Bar Chart Impl
        var energies = [];
        var dates = [];
      //  if(type==1){   
        for(var i in jsonResponse) {   
          if(jsonResponse[i].energy_total!=undefined && jsonResponse[i].date!=undefined && jsonResponse[i].energy_total>=0){ 
          if(i>0 && jsonResponse[i].date==jsonResponse[i-1].date){
              if(jsonResponse[i].energy_total<=jsonResponse[i-1].energy_total){
              i++;
              } else {
                console.log(jsonResponse[i].energy_total+"-->"+jsonResponse[i].date); 
                if(dates[dates.length-1]==jsonResponse[i].date){
                    jsonResponse[i].energy_total = parseFloat(jsonResponse[i].energy_total/1000.0);    
                    energies[energies.length-1]=jsonResponse[i].energy_total;
                    dates[dates.length-1]=jsonResponse[i].date;
                } else {
                    jsonResponse[i].energy_total = parseFloat(jsonResponse[i].energy_total/1000.0);
                    energies.push(jsonResponse[i].energy_total);
                    dates.push(jsonResponse[i].date); 
                }
              }
          } else { 
          jsonResponse[i].energy_total = parseFloat(jsonResponse[i].energy_total/1000.0);      
          energies.push(jsonResponse[i].energy_total);
          dates.push(jsonResponse[i].date);
          }
          }
          }
          var totalEnergy = 0;
          for(var i=0;i<=energies.length-1;i++){
             totalEnergy = totalEnergy + energies[i];
          }
        document.getElementById("energyTotals").innerHTML = ''+ totalEnergy.toFixed(2) + ' kWh';  
          $('#tableIdEnergy').DataTable( {
            "data" : jsonResponse,
            "columns" : [
                { "data": "date" },
                { "data": "energy_total"}
            ]
        } );
      //    } 
        //   else if(type==2){
        //     var today = new Date(); 
        //     var yyyy = today.getFullYear(); 
        //     for(var i in jsonResponse) { 
        //         if(jsonResponse[i].energyTotal!=undefined && jsonResponse[i].month!=undefined){ 
        //         if(i>0 && jsonResponse[i].month==jsonResponse[i-1].month){
        //             i++;
        //         } else {  
        //         lastFive = jsonResponse[i].month.substr(jsonResponse[i].month.length - 4);  
        //         if(yyyy==lastFive){
        //             energies.push(parseFloat(jsonResponse[i].energyTotal)/1000.0);
        //             dates.push(jsonResponse[i].month);     
        //         } 
        //         }
        //         }
        //         }
        //   } else {
        //     for(var i in jsonResponse) {   
        //         if(jsonResponse[i].energyTotal!=undefined && jsonResponse[i].year!=undefined){ 
        //         if(i>0 && jsonResponse[i].year==jsonResponse[i-1].year){
        //             i++;
        //         } else {   
        //         energies.push((parseFloat(jsonResponse[i].energyTotal)/1000.0));
        //         dates.push(jsonResponse[i].year);
        //         }
        //         }
        //         } 
        //   }



            // var today = new Date();
            // var dd = today.getDate();
            // var mm = today.getMonth()+1; //January is 0!
            // var yyyy = today.getFullYear();



         //  if(type==1){
            // if(dd<10){
            //       dd='0'+dd;
            //   } 
            //   if(mm<10){
            //       mm='0'+mm;
            //   } 
            //   dat = yyyy+'-'+mm+'-'+dd;
            //   energies.push(parseFloat(document.getElementById("kwhVal").value));
            //   dates.push(dat);




            // console.log(energies);
            // console.log(dates);
            // if(energies.length>1){
            //  for(var i=energies.length-1;i>0;i--){
            //   energies[i]=energies[i] - energies[i-1];
            //  }
            // }
            // if(dates.length>1){
            //     dates = dates.slice(1,dates.length);
            //     energies = energies.slice(1,energies.length);
            // }




            // var str=null;
            // for(i=0;i<dates.length;i++){
            //     str = dates[i].split("-");
            //     if(Number(str[1])==mm){
            //         dates = dates.slice(i,dates.length);
            //         energies=energies.slice(i,energies.length);
            //         break;
            //     } 
            // }
         //  } 
        //    if(type==2){
        //     var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        //     var d = new Date();
        //     //dates.push(monthNames[d.getMonth()]+"-"+d.getFullYear());
        //     //energies.push(parseFloat(document.getElementById("kwhVal").value));
        //     if(energies.length>1){
        //     for(var i=energies.length-1;i>0;i--){
        //       energies[i]=energies[i]-energies[i-1];
        //      }
        //     }
        //    }
        //    if(type==3){
        //      //dates.push(yyyy);
        //      //energies.push(parseFloat(document.getElementById("kwhVal").value));
        //      if(energies.length>1){
        //      for(var i=energies.length-1;i>0;i--){
        //       energies[i]=energies[i]-energies[i-1];
        //      }
        //     }
        //    }
           if(energies.length==0){
          //   if(type==1){
             alert("Daywise Data not available");
             return;
            // }
            //  if(type==2){
            //  alert("Monthwise Data not available");
            //  return;
            //  }
            //  if(type==3){
            //  alert("yearwise Data not available");
            //  return;
            //  }
            }
           $('#myAreaChart-energy').remove(); 
           $('#powerGraph').append('<div id="myAreaChart-energy" class="dashboard-flot-chart"></div>'); 

    //bar chart data
    Highcharts.chart('myAreaChart-energy', {
        chart: {
            type: 'column',
            zoomType: 'xy',
            spacingBottom: 30,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 40,
                viewDistance: 25
            }
        },
        title: {
            text: 'Energy'
        },
        subtitle: {
            text: 'Unit taken as kilowatt-hour',
            floating: true,
            align: 'right',
            verticalAlign: 'bottom',
            y: 15
        },
        xAxis: {
            categories:dates,
            labels: {
                skew3d: true,
            }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text:null
            },
            labels: {
                format: '{value:.2f}'
            },
            stackLabels: {
                enabled: false,
              //  format: '{value:.2f}',
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            verticalAlign: 'top',
            layout: 'vertical',
            x: 0,
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y:.2f}'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                depth: 40,
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            viewDeviceData(null,null,null,null,null,null,null,null,null,this.category);
                        }
                    }
                }
            }
        },
        series: [{
            name: 'ENERGY',
            data: energies,
            color:'#228B22'
        }]
    });
           }
         }      
       }
       function download(){
       var sensorType = document.getElementById("graphType").value;
        document.getElementById("selectdate").disabled = false;
        var device = document.getElementById("selectedDevice").value;
        var currentDate  = document.getElementById("selectdate").value;
        var splitsdate = currentDate.split('/');
        var finalDate = splitsdate[2]+"-"+splitsdate[0]+"-"+splitsdate[1];
        document.getElementById("displayDevice").innerHTML=document.getElementById("deviceSelect").innerHTML+"/Power";
        var userId = getCookie("userId");
        var jwt = getCookie("jwt");
        var phone = document.getElementById("phoneNumber").value;
        if(phone=="919717452684" && sensorType!="temperature"){
           device = device+"_dump";
        }
        var jsonRequest= {
                  "deviceId":device,
                  "jwt":jwt,
                  "userId":userId,
                  "date":finalDate
              };
            var data = JSON.stringify(jsonRequest);   
            var request= new XMLHttpRequest();  
            request.open("POST", "../sensor/getSensorData.php", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(data);
            request.onreadystatechange = function() {
              if (request.readyState == 4 && request.status === 200) { 
              var jsonResponse = JSON.parse(request.responseText);
                if(jsonResponse == '')
                    return;
        
        JSONToCSVConvertor(jsonResponse, "Report", true); 
       }
            }
       }  
 Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('');
};      
 function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
    
    var CSV = '';    
    //Set Report title in first row or line
    
    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";
        
        //This loop will extract the label from 1st index of on array
        for (var index in arrData[1]) {
            
            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);
        
        //append Label row with line break
        CSV += row + '\r\n';
    }
    
    //1st loop is to extract each row
    for (var i = 1; i < arrData.length; i++) {
        var row = "";
        
        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);
        
        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {        
        alert("Invalid data");
        return;
    }   
    
    //Generate a file name
    var date = new Date();
    var fileName = date.yyyymmdd();
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g,"_");   
    
    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    
    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    
    
    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");    
    link.href = uri;
    
    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";
    
    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
function convertSecond(hms){
    var a = hms.split(':'); // split it at the colons
    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
    return seconds;
}
function checkView(){
    var val = document.getElementById("realtime").value;
    if(val==1){
        document.getElementById("realtime").value = 0;
        val = 0;
    }
    else if(val==0) {
        document.getElementById("realtime").value = 1;
        val = 1;
    }
    if(val==0){
        document.getElementById( 'powerGraph' ).style.display = 'none';
        document.getElementById( 'tableViewPower' ).style.display = 'block';
    } else if(val==1){
        document.getElementById( 'powerGraph' ).style.display = 'block';
        document.getElementById( 'tableViewPower' ).style.display = 'none';
    }
}
