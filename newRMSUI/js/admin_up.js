var edit = false;
var editId = null;

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
       c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
    }
}
return "";
}

function addEditDevice(){
    var userId = getCookie("userId");
    var jwt = getCookie("jwt");
    console.log("factor:"+document.getElementById("multiplicationFactor").value);
    if(edit==false){
        var deviceName = document.getElementById("deviceName").value;
        var deviceId = document.getElementById("deviceId").value;  
        var devicePwd = document.getElementById("devicepwd").value;  
        var city = document.getElementById("city").value;
        var country = document.getElementById("country").value;
        var installation_date = document.getElementById("installation_date").value;
        var sensorType = document.getElementById("sensorType").value;
        var multiplicationFactor = document.getElementById("multiplicationFactor").value;
        var systemSize = document.getElementById("systemSize").value;
        var version = document.getElementById("version").value;
        var terrif = document.getElementById("terrif").value;
        var deviceDetails = document.getElementById("deviceDetails").value; 
        var jsonRequest= {
            "deviceName":deviceName,
            "deviceId":deviceId,
            "devicePassword":devicePwd,
            "jwt":jwt,
            "userId":userId,
            "city":city,
            "country":country,
            "installation_date":installation_date,
            "sensorType":sensorType,
            "mutiFactor":multiplicationFactor,
            "systemSize":systemSize,
            "version":version,
            "terrif":terrif,
            "deviceDetail":deviceDetails
        };
      var data = JSON.stringify(jsonRequest);  
      var request= new XMLHttpRequest();  
      request.open("POST", "../sensor/Admin/addNewDevice.php", true);
      request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      request.send(data);
      request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status === 200) {
        var jsonResponse = JSON.parse(request.responseText);
           alert(jsonResponse.message);
           window.location.reload();
          }
      }
    }
    if(edit==true){
        var deviceName = document.getElementById("deviceName").value;
        var deviceId = document.getElementById("deviceId").value; 
     //   var devicePwd = document.getElementById("devicepwd").value;  
        var city = document.getElementById("city").value;
        var country = document.getElementById("country").value;
        var installation_date = document.getElementById("installation_date").value;
        var sensorType = document.getElementById("sensorType").value;
        var multiplicationFactor = document.getElementById("multiplicationFactor").value;
         console.log("factor:"+document.getElementById("multiplicationFactor").value);
        console.log("factor:"+multiplicationFactor);
        var systemSize = document.getElementById("systemSize").value;
        var deviceDetails = document.getElementById("deviceDetails").value; 
        var version = document.getElementById("version").value;
        var terrif = document.getElementById("terrif").value;
        var jsonRequest= {
            "id":editId,
            "deviceName":deviceName,
            "deviceId":deviceId,
            "jwt":jwt,
            "userId":userId,
            "city":city,
            "country":country,
            "installation_date":installation_date,
            "sensorType":sensorType,
            "mutiFactor":multiplicationFactor,
            "systemSize":systemSize,
            "version":version,
            "terrif":terrif,
            "deviceDetail":deviceDetails
        };
      var data = JSON.stringify(jsonRequest);  
      var request= new XMLHttpRequest();  
      request.open("POST", "../sensor/Admin/editDevice.php", true);
      request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      request.send(data);
      request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status === 200) {
        var jsonResponse = JSON.parse(request.responseText);
           alert(jsonResponse.message);
           window.location.reload();
          }
      }
    }
    }

function editTemp(id){
    document.getElementById( 'passwordSection' ).style.display = 'none'; 
    edit = true;
    editId = id;
    var userId = getCookie("userId");
    var jwt = getCookie("jwt");
    var jsonRequest= {
        "id":editId,
        "userId":userId,
        "jwt":jwt,
    };
    var data = JSON.stringify(jsonRequest);  
      var request= new XMLHttpRequest();  
      request.open("POST", "../sensor/Admin/getDeviceDetails.php", true);
      request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      request.send(data);
      request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status === 200) {
            console.log(request.responseText);
            var jsonResponse = JSON.parse(request.responseText);
            console.log(jsonResponse);
        var message = jsonResponse.message;
        if(message=='success'){ 
          var deviceVal = jsonResponse.device;
          console.log(deviceVal);
          document.getElementById("deviceName").value = deviceVal['deviceName'];
          document.getElementById("deviceId").value = deviceVal['deviceId'];
          document.getElementById("city").value = deviceVal['city'];
          document.getElementById("country").value = deviceVal['country'];
          document.getElementById("installation_date").value = deviceVal['installation_date'];
          document.getElementById("sensorType").value = deviceVal['sensorType'];
          document.getElementById("multiplicationFactor").value = deviceVal['mutiFactor'];
          document.getElementById("systemSize").value = deviceVal['systemSize'];
          document.getElementById("version").value = deviceVal['version'];
          document.getElementById("terrif").value = deviceVal['terrif'];
        }   
    }

}
}
function deleteTemp(id){
    editId = id;
   console.log(id);
}
function deleteDevice(){
    var userId = getCookie("userId");
    var jwt = getCookie("jwt");
    var jsonRequest= {
        "id":editId,
        "userId":userId,
        "jwt":jwt,
    };
    console.log(editId);
    var data = JSON.stringify(jsonRequest);  
      var request= new XMLHttpRequest();  
      request.open("POST", "../sensor/Admin/deleteDevice.php", true);
      request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      request.send(data);
      request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status === 200) {
            console.log(request.responseText);
        var jsonResponse = JSON.parse(request.responseText);
        console.log(jsonResponse);
           alert(jsonResponse.message);
           window.location.reload();
        }
    }
}

function createTable(jsonResponse) {
    //var table = document.getElementById('energyTable');
    for(var i in jsonResponse) {
    
        var tr="<tr>";
        // var td1="<td>"+jsonResponse[i]._id +"</td>";
        var td2="<td>"+jsonResponse[i].energy_total+"</td>";
        var td3="<td>"+jsonResponse[i].date+"</td>"
        +"</tr>";
       $("#energyTable").append(tr+td2+td3);  
  }
}
function getEnergy(startDt,endDate){
    var deviceId = document.getElementById("selectedDevice").value;
    var userId = getCookie("userId");
     $('#energyTable').DataTable().destroy();
    var jwt = getCookie("jwt");
    var jsonRequest= {
        "deviceId":deviceId,
        "jwt":jwt,
        "userId":userId,
        "startDate":startDt,
        "endDate":endDate
    }; 
    var data = JSON.stringify(jsonRequest);   
    var request= new XMLHttpRequest();  
    request.open("POST", "../sensor/getEnergyByDateRange.php", true);    
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(data);
    request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status === 200) {
    var jsonResponse = JSON.parse(request.responseText);
    createTable(jsonResponse);
    $('#energyTable').DataTable();
    }
  } 
}
function sendNotifications(id,etoday){
    etoday = parseFloat(etoday/1000.0).toFixed(2);
   // etoday = parseFloat(etotal/1000.0).toFixed(2);
   var textVal = document.getElementById("notificationText").value;
   var n = textVal.search("{}");
   var m = textVal.search("()");
   var notificationText='';
   if(n!=-1){
     notificationText = textVal.replace("{}", etoday);
      textVal = notificationText;
   }
   if(m!=-1){
     var money = (etoday*9.0).toFixed(2);
     notificationText = textVal.replace("()", money); 
   }
    var userId = getCookie("userId");
    var jwt = getCookie("jwt");
     var jsonRequest= {
        "deviceId":id,
        "jwt":jwt,
        "userId":userId,
        "notificationText":notificationText
    };
    var data = JSON.stringify(jsonRequest);   
    var request= new XMLHttpRequest();  
    request.open("POST", "../sensor/getPhnNumbersByDeviceId.php", true);    
    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    request.send(data);
    request.onreadystatechange = function() {
    if (request.readyState == 4 && request.status === 200) {
         var jsonResponse = JSON.parse(request.responseText); 
         alert(jsonResponse.message); 
    }
  } 
}
function resetVal(){
    document.getElementById("deviceName").value = null;
          document.getElementById("deviceId").value = null;
          document.getElementById("city").value = null;
          document.getElementById("country").value = null;
          document.getElementById("installation_date").value =null;
          document.getElementById("sensorType").value = null;
          document.getElementById("multiplicationFactor").value = null;
          document.getElementById("systemSize").value = null;
          document.getElementById("version").value = null;
          document.getElementById("terrif").value = null;
}