<?php
if(!isset($_COOKIE['userId']) || !isset($_COOKIE['jwt'])){
  header('Location:./login.php');
} else {
require '../auth/src/authentication/validUser.php';  
$validate = ValidateUser::validLogin($_COOKIE['jwt'],$_COOKIE['userId']);
$deviceList = array();
$userdevicemap=array();
if($validate){
  $config['db']['host'] = "localhost";
  $config['db']['username']  = "root";
  $config['db']['password'] = "zunganman@MR123";
  $config['db']['database'] = "zunpulse";
  $config['jwt']['key'] = 'D809526552FBF43AB6748A69FBAF99BCABD77390F1D516D9A0E355D70DC4B576';
  $conn = MyDatabase::connect($config['db']['host'],$config['db']['database'],$config['db']['username'],$config['db']['password']);
    $admin = true;
    $user = $conn->prepare('SELECT *FROM users WHERE id=:usid AND isAdmin=:isAdmin');
    $user->bindParam(':usid',$_COOKIE['userId']);
    $user->bindParam(':isAdmin',$admin);
    $user->execute();
    if($user->rowCount()>0){
      $query1 = $conn->prepare('SELECT id,deviceName,deviceDetail,country,deviceId,installation_date,city,sensorType,mutiFactor,version,systemSize,terrif,etoday,etotal FROM devicedetail');
      $query1->execute();
      $deviceList = $query1->fetchAll(PDO::FETCH_ASSOC);
      $query2=$conn->prepare('SELECT u.phone,m.deviceId,m.deviceName from users u JOIN userdevicemapping m ON u.id=m.userid');
      $query2->execute();
      $userdevicemap=$query2->fetchAll(PDO::FETCH_ASSOC);
    } else {
        header('Location:./profile.php');
        exit();  
    }

} else {
  header('Location:./login.php');
  exit();
}
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
    <title>Zunroof RMS</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.8.2/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
   <!-- Bootstrap Core Css -->
   <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" /> -->
   <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />

 <!-- JQuery DataTable Css -->
 <!-- <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"> -->
 <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
 <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<!-- Waves Effect Css -->
<link href="plugins/node-waves/waves.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="plugins/animate-css/animate.css" rel="stylesheet" />

<!-- Morris Chart Css-->
<link href="plugins/morrisjs/morris.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="css/style.css" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="profile.php">ZunRMS (Admin)</a>
            </div>
         <input type="text" value="" id="phoneNumber" style="display:none">
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>4 sales made</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 22 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy Doe</b> deleted account</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy</b> changed name</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> commented your post</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">cached</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> updated status</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">settings</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Settings updated</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> Yesterday
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="displayDevice"></div>
                    <div class="email"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal">
                                    <i class="material-icons">input</i>Sign Out</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">NAVIGATION</li>
                    <li class="active">
                        <a href="profile.php">
                            <i class="material-icons">accessibility</i>
                            <span>Admin</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#addDevice">
                            <i class="material-icons">add_circle_outline</i>
                            <span>Add Device</span>
                        </a>
                    </li>
                    <li>
                        <a href="profile.php">
                            <i class="material-icons">home</i>
                            <span>RMS View</span>
                        </a>
                    </li>
                    <li>
                    <a href="javascript:void(0);" style="display:none">
                            <i class="material-icons">iso</i>
                            <span id="deviceSelect"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">iso</i>
                            <span>Device List</span>
                        </a>
                        <ul class="ml-menu"> 
                                    <li>
                                      hello
                                   </li> 
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 - 2019
                    <a href="javascript:void(0);">ZunRoof Tech Private Limited</a>.
                </div>
                <div class="version">
                    <b>All Right Reserved.</b>
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid" style="margin-top:4px;">
            <!-- Widgets -->
            <div class="row clearfix"> 
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="body" style="">
                        </div>
                </div>
            </div>
            <!-- #END# Widgets -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Device List
                                <small>You edit ,delete and send notification by dragging horizontal scroller.</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Add Device</a></li>
                                        <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table id="tableDevice" class="display" style="width:100%" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Device ID</th>
                                        <th>Device Name</th>
                                        <th>City</th>
                                        <th>Sensor Type</th>
                                        <th>Multiplecation Factor</th>
                                        <th>System Size</th>
                                        <th>Version</th>
                                        <th>etoday</th>
                                        <th>Action</th>
                                        <th>SEND NOTIFICATIONS</th>
                                        <th>AUTO NOTIFICATION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php  foreach($deviceList as $key => $val) { ?>	
                                    <tr>
                                        <td><a href="javascript:void(0);" data-toggle="modal" data-target="#showAdminDevice"><?=$val['id']?></a></td>
                                        <td><?=$val['deviceId']?></td>
                                        <td><?=$val['deviceName']?></td>
                                        <td><?=$val['city']?></td>
                                        <td><?=$val['sensorType']?></td>
                                        <td><?=$val['mutiFactor']?></td>
                                        <td><?=$val['systemSize']?></td>
                                        <td><?=$val['version']?></td>
                                        <td><?=$val['etoday']/1000?></td>
                                        <td>
                                        <?php echo '
                                     <a href="javascript:void(0)"  class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#addDevice"
                                    onclick="editTemp(\''.$val["id"].'\')"></a><a href="javascript:void(0)" style="margin-left:10%" class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteModal" onclick="deleteTemp(\''.$val["id"].'\')"';
                                   ?> 
                                   </td>
                                   <td>
                                    <?php if($val['sensorType']=='RMS'){
                                         echo '
                                     <a href="javascript:void(0)"  class="glyphicon glyphicon-send"
                                    onclick="sendNotifications(\''.$val['deviceId'].'\',\''.$val['etoday'].'\')"></a>';
                                   ?> 
                                        <?php } ?>           
                                   </td>
                                   <td>
                                   <?php echo '<div class="switch">
                                   <input id=\''.$val['deviceId'].'\' class="cmn-toggle cmn-toggle-round"  type="checkbox">
                                   <label for=\''.$val['deviceId'].'\'></label>
                                   </div>';
                                   ?>
                                   </td>
                                 </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                User-Device List
                                
                            </h2>
                            <!--<ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Add Device</a></li>
                                        <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> 
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body table-responsive">
                            <table id="userDevice" class="display" style="width:100%" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Device ID</th>
                                        <th>Device Name</th>
                                        <th>Phone Number</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php  foreach($userdevicemap as $key2 => $val2) { ?>	
                                    <tr>
                                        
                                        <td><?=$val2['deviceId']?></td>
                                        <td><?=$val2['deviceName']?></td>
                                        <td><?=$val2['phone']?></td>
                                        
                                 </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="body">
                            <h2 class="card-inside-title">Type Notification</h2>
                <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" id="notificationText" placeholder="please use '{}' for energy and '()' for money"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
            <!-- #END# Striped Rows -->
            <div class="row clearfix">
              <div class="col-sm-6">
                <select class="form-control show-tick" id="selectedDevice">      
                    <?php foreach($deviceList as $key => $val){
                    echo "<option value='" . $val['deviceId'] . "'>" . $val['deviceId'] .'--'. $val['deviceName'] . "</option>";
                     } ?>    
                </select>
            </div>
        </div>
        <div class="row clearfix">
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; margin-bottom:5px; width: 40%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span> <b class="caret"></b>
                                 </div> 
       </div>
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Energy Details</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="energyTable">
                                    <thead>
                                        <tr>
                                            <th>Total Energy</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                 </table>   
                            </div>
                        </div>
                    </div>
                </div>

              
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h2>BROWSER USAGE</h2>
                        </div>
                        <div class="body">
                            <div id="donut_chart" class="dashboard-donut-chart"></div>
                        </div>
                    </div>
                </div> -->
                <!-- #END# Browser Usage -->
            </div>
        </div>
        <div class="modal fade" id="updateTerrif" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Update Electricity Unit</h4>
                        </div>
                        <div class="modal-body">
                                <form>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceidUp" class="form-control">
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" id="ebTerif" class="form-control"
                                                        required>
                                                        <label class="form-label"><strong style="color:red">*</strong>assumption ₹9 per kHhr</label>
                                                    </div>
                                                </div>
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" onclick="updateTerrif()" >SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="modal fade" id="addDevice" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Add Device</h4>
                        </div>
                        <div class="modal-body">
                                <form>
                                        <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceName" class="form-control" required>
                                                    <label class="form-label" >Device Name</label>
                                                </div>
                                            </div>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceId" class="form-control">
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div id = "passwordSection" class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="password" id="devicepwd" class="form-control" required>
                                                        <label class="form-label">Device Password</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" id="deviceDetails" class="form-control" required>
                                                        <label class="form-label">Device Details</label>
                                                    </div>
                                                </div>    
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                    <div class="row-fluid">
                                                    <select style="margin: 10px auto;" class="selectpicker" 
                                                    data-show-subtext="true" 
                                                    data-live-search="true" data-style="btn-success"  id="city" required>
                     <option data-tokens="del" value="delhi">DELHI</option> 
<option data-tokens="gur" value="gurgaon">GURGAON</option> 
<option data-tokens="noi" value="noida">NOIDA</option> 
<option data-tokens="gha" value="ghaziabad">GHAZIABAD</option>  
<option data-tokens="far" value="faridabad">FARIDABAD</option>  
<option data-tokens="agr" value="agra">AGRA</option>  
<option data-tokens="chan" value="chandigarh">CHANDIGARH</option>   
              </select>
              </div>
                                                    </div>
                                                </div>    
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" id="country" value="India" class="form-control" readonly required>
                                                        <label class="form-label">Country</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" id="installation_date" class="form-control" required>
                                                        <label class="form-label">Installation Date</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" id="sensorType" class="form-control" required>
                                                        <label class="form-label">Sensor Type</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" id="multiplicationFactor" class="form-control" required>
                                                        <label class="form-label">Multiplecation Factor</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" id="systemSize" class="form-control" required>
                                                        <label class="form-label">System Size</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" id="version" class="form-control" required>
                                                        <label class="form-label">Version</label>
                                                    </div>
                                                </div> 
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" id="terrif" class="form-control" required>
                                                        <label class="form-label">Terrif</label>
                                                    </div>
                                                </div> 
                                                                                
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" onclick="addEditDevice()" >SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" onclick="resetVal()" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>


 <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Are you Sure ?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <!-- <input type="text" style="display:none" id="kwhVal"> -->
                          </button>
                        </div>
                        <div class="modal-body">Click on delete to go ahead.</div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-primary" href="javascript:void(0)" onclick="deleteDevice()">Delete</a>
                        </div>
                      </div>
                    </div>
                  </div>



            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave ?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <!-- <input type="text" style="display:none" id="kwhVal"> -->
                          </button>
                        </div>
                        <div class="modal-body">Select "LogOut" to end your current session.</div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-primary" href="../auth/src/authentication/logout.php">Logout</a>
                        </div>
                      </div>
                    </div>
                  </div>
            <div class="modal fade" id="showAdminDevice" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Device Information</h4>
                        </div>
                        <div class="modal-body">
                        <div class="row clearfix">
              <div class="col-sm-6">
                <select class="form-control show-tick" id="selectedDevice">      
                    <?php foreach($deviceList as $key => $val){
                    echo "<option value='" . $val['deviceId'] . "'>" . $val['deviceId'] .'--'. $val['deviceName'] . "</option>";
                     } ?>    
                </select>
            </div>
        </div>
        <div class="row clearfix">
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; margin-bottom:5px; width: 40%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span> <b class="caret"></b>
                                 </div> 
       </div>
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Energy Details</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos" id="energyTable">
                                    <thead>
                                        <tr>
                                            <th>Total Energy</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                 </table>   
                            </div>
                        </div>
                    </div>
                </div>

              
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h2>BROWSER USAGE</h2>
                        </div>
                        <div class="body">
                            <div id="donut_chart" class="dashboard-donut-chart"></div>
                        </div>
                    </div>
                </div> -->
                <!-- #END# Browser Usage -->
            </div>
        </div>
                         
                                           
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
           
    </section>

    <script src="plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
<!-- Bootstrap Core Js -->
<script src="plugins/bootstrap/js/bootstrap.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!--Date Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/gijgo@1.8.2/combined/js/gijgo.min.js" type="text/javascript"></script> -->
<!-- Select Plugin Js -->
<script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/morrisjs/morris.js"></script>
<!-- ChartJs -->
<script src="plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="plugins/flot-charts/jquery.flot.js"></script>
<script src="plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<!-- Custom Js -->


    <!-- Custom Js -->
    <script src="js/admin_up.js"></script>
    <script src="js/admin.js"></script>
<!-- Demo Js -->
<script src="js/demo.js"></script>
<script>
        $(document).ready(function () {
         //   var val = document.getElementById("realtime").value;
            var now = new Date();
            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);
            var today = (month) + "/" + (day) + "/" + now.getFullYear();
           // $('#selectdate').datepicker();
            $('#selectdate').val(today);
            $('#tableDevice').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    {
                          extend: 'colvis',
                          columns: ':gt(0)'
                    }
                ]
            });
            $('#userDevice').DataTable();
        });   
        // $(document).click(function (e) {
        //  e.stopPropagation();
        //  var container = $(".dropdown");
        //  if (container.has(e.target).length === 0) {
        //    $('.dropdown').removeClass("open");  
        // }
        // });
$(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D') + '-' + end.format('MMMM D-YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        showDropdowns:true,
        linkedCalendars:false,
        maxDate: moment(),
        opens: 'center',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);
});
$('#reportrange').blur(function(){
        console.log(($('#reportrange').data('daterangepicker').startDate._d).getDate());
    });

$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
  var startDate = picker.startDate;
  var endDate = picker.endDate;
    getEnergy(startDate.format('YYYY-MM-DD'),endDate.format('YYYY-MM-DD'));
});  
    </script>
</body>

</html>