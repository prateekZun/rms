﻿<?php
if(!isset($_COOKIE['userId']) || !isset($_COOKIE['jwt'])){
  header('Location:./login.php');
} else {
require '../auth/src/authentication/validUser.php';  
$validate = ValidateUser::validLogin($_COOKIE['jwt'],$_COOKIE['userId']);
$deviceList = array();
if($validate){
  $config['db']['host'] = "localhost";
  $config['db']['username']  = "root";
  $config['db']['password'] = "zunganman@MR123";
  $config['db']['database'] = "zunpulse";
  $config['jwt']['key'] = 'D809526552FBF43AB6748A69FBAF99BCABD77390F1D516D9A0E355D70DC4B576';
  $conn = MyDatabase::connect($config['db']['host'],$config['db']['database'],$config['db']['username'],$config['db']['password']);
    $query = $conn->prepare('SELECT *FROM userdevicemapping WHERE userid=:uid');
    $query->bindParam(':uid',$_COOKIE['userId']);
    $query->execute();
    if($query->rowCount()>0){
    $rslList = $query->fetchAll(PDO::FETCH_ASSOC);
    $user = $conn->prepare('SELECT *FROM users WHERE id=:usid');
    $user->bindParam(':usid',$_COOKIE['userId']);
    $user->execute();
    $detailUser=$user->fetch(PDO::FETCH_ASSOC);
    foreach($rslList as $id => $value){
      $query1 = $conn->prepare('SELECT u.deviceName,d.deviceId,d.installation_date,d.city,d.sensorType,d.mutiFactor,d.version,d.systemSize,d.terrif FROM devicedetail d,userdevicemapping u where d.deviceId=:deviceId AND u.userid=:userId AND u.deviceId=:deviceId2');
      $query1->bindParam(':deviceId',$value['deviceId']);
      $query1->bindParam(':deviceId2',$value['deviceId']);
      $query1->bindParam(':userId',$_COOKIE['userId']);
      if($query1->execute()){
          array_push($deviceList,$query1->fetch(PDO::FETCH_ASSOC));
      }
    }
  } else {
    $deviceList = null;
  }
} else {
  header('Location:./login.php');
  exit();
}
$q = $conn->prepare('SELECT *FROM notification WHERE phn=:phn');
$q->bindParam(':phn',$detailUser['phone']);
$notificationCount = 0;
if($q->execute()){
    $notificationList = $q->fetchAll(PDO::FETCH_ASSOC);
    $notificationCount = count($notificationList);
}
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv='cache-control' content='no-cache'>
     <meta http-equiv='expires' content='0'>
     <meta http-equiv='pragma' content='no-cache'>
    <title>Zunroof RMS</title>
    <!-- Favicon-->
    <link rel="shortcut icon" href="">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.8.2/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
   <!-- Bootstrap Core Css -->
   <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" /> -->
   <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.css" />

 <!-- JQuery DataTable Css -->
 <!-- <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"> -->
 <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
 <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<!-- Waves Effect Css -->
<link href="plugins/node-waves/waves.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="plugins/animate-css/animate.css" rel="stylesheet" />

<!-- Morris Chart Css-->
<link href="plugins/morrisjs/morris.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="css/style.css" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="profile.php">ZunRMS (β)</a>
            </div>
         <input type="text" value="<?php echo $detailUser['phone'] ?>" id="phoneNumber" style="display:none">
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count"><?=$notificationCount?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                <?php foreach($notificationList as $key => $v){?>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="menu-info">
                                            <i class="material-icons">notifications_active</i><h4><?=$v['deviceId']?>:</h4> <h4><?=$v['notification_text']?></h4>
                                                <p>
                                                    <i class="material-icons">access_time</i><?=$v['date']?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li class="footer">
                            <a href="javascript:void(0);" onclick="clearNotification()">Clear All</a>

                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="displayDevice"></div>
                    <div class="email"><?=$detailUser['phone']?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                          <li>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#updateDeviceName">
                                <i class="material-icons">rowing</i>Update Name</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#changePassword">
                                <i class="material-icons">change_history</i>Change Password</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#deleteDevice">
                                <i class="material-icons">delete_forever</i>Delete Device</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal">
                                    <i class="material-icons">input</i>Sign Out</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">NAVIGATION</li>
                    <li class="active">
                        <a href="profile.php">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#addDevice">
                            <i class="material-icons">add_circle_outline</i>
                            <span>Add Device</span>
                        </a>
                    </li>
                    <li>
                    <a href="javascript:void(0);" style="display:none">
                            <i class="material-icons">iso</i>
                            <span id="deviceSelect"></span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">iso</i>
                            <span>Device List</span>
                        </a>
                        <ul class="ml-menu"> 
                                 <?php if($deviceList!=NULL){
                                 foreach($deviceList as $key => $val){?>
                                    <li>
                                   <?php echo '
                                     <a href="javascript:void(0)"
                                    onclick="viewDeviceData(\''.$val["deviceId"].'\',\''.$val["deviceName"].'\',\''.$val["city"].'\',\''.$val["version"].'\',\''.$val["sensorType"].'\',\''.$val["mutiFactor"].'\',\''.$val["systemSize"].'\',\''.$val["terrif"].'\',\''.$val["installation_date"].'\',null)"><i class="material-icons">devices</i> <span>'.$val["deviceName"].'</span></a> ';
                                   ?> 
                                   </li> 
                             <?php }} ?> 
                             
                             <input type="text" id="selectedDevice" value="<?php echo $deviceList[0]['deviceId'] ?>" style="display:none"></input>
                                   <input type="text" id="selectedDeviceName" value="<?php echo $deviceList[0]['deviceName'] ?>" style="display:none"></input>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 - 2019
                    <a href="javascript:void(0);">ZunRoof Tech Private Limited</a>.
                </div>
                <div class="version">
                    <b>All Right Reserved.</b>
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid" style="margin-top:4px;">
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">
                                <img src="images/solar-energy.png" height="45px" width="45px">
                            </i>
                        </div>
                        <div class="content" id="total_EBlock">
                            <div class="text">Energy (Today)</div>
                            <div class="number count-to" id="total_E" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                    <input type="text" style="display:none" id="kwhVal">
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">
                                <img src="images/green-energy.png" height="45px" width="45px">
                            </i>
                        </div>
                        <div class="content" id="powerBlock">
                            <div class="text">Power (Now)</div>
                            <div class="number count-to" data-speed="1000" id="power" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="moneyTitle">
                    <div class="info-box bg-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">
                                <img src="images/coins.png" height="45px" width="45px">
                            </i>
                        </div>
                        <div class="content">
                            <div class="text">Money Saved (Total)</div>
                            <div class="number count-to" data-speed="1000" id="moneySaved" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <center>
                         <input type="text" id="graphType" value="<?php echo $deviceList[0]['sensorType'] ?>" style="display:none">
                        <input type="text" id="multiFactor" value="<?php echo $deviceList[0]['mutiFactor'] ?>" style="display:none">
                    <input style="color:green;display:none" id="selectdate" onchange="viewDeviceData(null,null,null,null,null,null,null,null,null,null)"/>
                </center>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   
                        <div class="body" style="">
                        <center>
                            <!-- <div class="button-demo"> -->
                            <div id="reportrange" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; margin-bottom:5px; width: 100%">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
    <span></span> <b class="caret"></b>
                                 </div>  
                                <!-- <button type="button" class="btn btn-success waves-effect" onclick="getDayMonthYearView(1)">This Month</button>
                                <button type="button" class="btn btn-success waves-effect" onclick="getDayMonthYearView(1)">Last Month</button> -->
                                <!-- <button type="button" class="btn btn-success waves-effect" onclick="getDayMonthYearView(2,null,null)">Year</button>
                                <button type="button" class="btn btn-success waves-effect" onclick="getDayMonthYearView(3,null,null)">Total</button> -->
                            <!-- </div> -->
                            </center>
                        </div>
                    
                </div>
            </div>
            <!-- #END# Widgets -->
            <!-- CPU Usage -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2 id="labelpe">Power</h2>
                                </div>
                                <div class="col-xs-12 col-sm-6 align-right">
                                    <div class="switch panel-switch-btn">
                                        <span class="m-r-10 font-12"></span>
                                        <label>Table View
                                            <input type="checkbox" id="realtime" value=1 checked onchange="checkView()">
                                            <span class="lever switch-col-cyan"></span>Graph View</label>
                                    </div>
                                </div>
                            </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" onclick="download()">Download</a></li>
                                        <li><a href="javascript:void(0);"  data-toggle="modal" data-target="#updateTerrif" onclick="updateTerrif()">Update Electricity Rate</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body" id="powerGraph">
                                <center>
                                        <div class="loader" id="loader">
                                        </div>
                                </center>
                                <div id="myAreaChart-power" class="dashboard-flot-chart"></div> 
                                <div id="myAreaChart-energy" class="dashboard-flot-chart"></div>
                                <table id="example" class="display" width="100%"></table>


                         </div>
                         <div class="body" id="tableViewPower" style="display:none">
                         <table id="tableId" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Energy</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Date</th>
                <th>Energy</th>
            </tr>
        </tfoot>
        <tbody>
        <tr>
           <th id="dateFin">
           </th>
           <th id="energyFin">
           </th>
        </tr>
        </tbody>
    </table>
                         
                         <table id="tableIdEnergy" class="display" style="width:100%;display:none">
        <thead>
            <tr>
                <th>Date</th>
                <th>Energy</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Total</th>
                <th id="energyTotals"></th>
            </tr>
        </tfoot>
    </table>
    </div>
                        <style>

                                /* Rotation */
                          @keyframes rotate {
                            from {transform: rotate(0deg);}
                            to {transform: rotate(360deg);}
                          }
                          
                          /* Square */
                          #loader, 
                          #loader:before,
                          #loader:after {
                          width: 4em;
                          height: 4em;
                          content: "";
                          background-color: #228B22; 
                          }
                          
                          /* Position */
                          #loader {
                          margin: 4em auto;
                          position: relative;
                          }
                          
                          /* Animate */
                          #loader {
                          animation: rotate 2.5s infinite linear;
                          }
                          
                          /* Position */
                          #loader:before,
                          #loader:after {
                          position: absolute;
                          left: 0;
                          top: 0;
                          }
                          
                          /* Star shape */
                          #loader:before {
                           transform: rotate(60deg);
                          }
                          #loader:after {
                           transform: rotate(-60deg);
                          }
                          
                                  </style>  
                    </div>
                </div>
            </div>
            <!-- #END# CPU Usage -->
            <div class="row clearfix">
                <!-- Visitors -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-green">
                            <div class="sparkline" data-type="line" data-spot-Radius="1">
                                <img class="headerImg " src="images/weather.png" height="45px" width="45px"> Weather Details
                            </div>
                            <ul class="dashboard-stat-list">
                            <li>
                                    Weather:
                                    <span class="pull-right">
                                        <b id="weather"></b>
                                    </span>
                                </li>
                                <li>
                                    Temperature:
                                    <span class="pull-right">
                                        <b id="temp"></b>
                                    </span>
                                </li>
                                <li>
                                    Pressure:
                                    <span class="pull-right">
                                        <b id="pressure"></b>
                                    </span>
                                </li>
                                <li>
                                    Humidity:
                                    <span class="pull-right">
                                        <b id="humidity1"></b>
                                    </span>
                                </li>
                                <li>
                                    Sunrise:
                                    <span class="pull-right">
                                        <b id="sunrise"></b>
                                    </span>
                                </li>
                                <li>
                                    Sunset:
                                    <span class="pull-right">
                                        <b id="sunset"></b>
                                    </span>
                                </li>
                                <li>
                                    Visibility:
                                    <span class="pull-right">
                                        <b id="visibility"></b>
                                    </span>
                                </li>
                                <li>
                                    Clouds:
                                    <span class="pull-right">
                                        <b id="clouds"></b>
                                    </span>
                                </li>
                                <li>
                                    Wind Speed:
                                    <span class="pull-right">
                                        <b id="windspeed"></b>
                                    </span>
                                </li>
                                <li>
                                    Wind Direction:
                                    <span class="pull-right">
                                        <b id="windDirection"></b>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Visitors -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-green">
                            <div class="sparkline" data-type="line" data-spot-Radius="1">
                                <img src="images/plant.png" height="45px" width="45px"> Plant Details</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Name:
                                    <span class="pull-right">
                                        <b id="deviceNam"></b>
                                    </span>
                                </li>
                                <li>
                                    User Id:
                                    <span class="pull-right">
                                        <b><?=$detailUser['id']?></b>
                                    </span>
                                </li>
                                <li>
                                    Username:
                                    <span class="pull-right">
                                        <b><?=$detailUser['phone']?></b>
                                    </span>
                                </li>
                                <li>
                                    Install Date:
                                    <span class="pull-right">
                                           <b id="installation"></b>
                                           <input id="installation_date" value="<?php echo $deviceList[0]['installation_date']?>" style="display:none"></input>
                                    </span>
                                </li>
                                <li>
                                    Type:
                                    <span class="pull-right">
                                        <b>Grid-Tied</b>
                                    </span>
                                </li>
                                <li>
                                    City:
                                    <span class="pull-right">
                                        <b id="cityName"></b>
                                    </span>
                                    <input id="city" value="<?php echo $deviceList[0]['city']?>" style="display:none"></input>
                                </li>
                                <li>
                                    System Size:
                                    <span class="pull-right">
                                        <b id="sysSize"></b>
                                        <input id="systemSize" value="<?php echo $deviceList[0]['systemSize']?>" style="display:none"></input>
                                    </span>
                                </li>
                                <li>
                                    Electricity Rate:
                                    <span class="pull-right">
                                    <b><?=$deviceList[0]['terrif']?></b>
                                    </span>
                                    <input id="terrif" value="<?php echo $deviceList[0]['terrif']?>" style="display:none"></input>
                                </li>
                                <li>
                                    Country:
                                    <span class="pull-right">
                                    <b>India</b>
                                    </span>
                                </li>
                                <li>
                                    Version:
                                    <span class="pull-right">
                                    <b id="vers"></b>
                                    </span>
                                    <input id="version" value="<?php echo $deviceList[0]['version']?>" style="display:none"></input>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
                 <!-- Latest Social Trends -->
                 <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-green">
                            <div class="sparkline" data-type="line" data-spot-Radius="1">
                                <img class="headerImg" src="images/otherRev.png" height="45px" width="45px"> Other Revenue
                            </div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Money Saved
                                    <span class="pull-right">
                                        <b id="moneySavedDn"></b>
                                    </span>
                                </li>
                                <li>
                                    Co2 Reduced
                                    <span class="pull-right">
                                        <b id="co2Reduced"></b>
                                    </span>
                                </li>
                                <li>
                                    Coal Saved
                                    <span class="pull-right">
                                        <b id="coalSaved"></b>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Latest Social Trends -->
            </div>

            <div class="row clearfix" style="display:none">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>TASK INFOS</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Time</th>
                                            <th>Power</th>
                                            <th>Total Energy</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Task A</td>
                                            <td>
                                                <span class="label bg-green">Doing</span>
                                            </td>
                                            <td>John Doe</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Task B</td>
                                            <td>
                                                <span class="label bg-blue">To Do</span>
                                            </td>
                                            <td>John Doe</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Task C</td>
                                            <td>
                                                <span class="label bg-light-blue">On Hold</span>
                                            </td>
                                            <td>John Doe</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Task D</td>
                                            <td>
                                                <span class="label bg-orange">Wait Approvel</span>
                                            </td>
                                            <td>John Doe</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Task E</td>
                                            <td>
                                                <span class="label bg-red">Suspended</span>
                                            </td>
                                            <td>John Doe</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h2>BROWSER USAGE</h2>
                        </div>
                        <div class="body">
                            <div id="donut_chart" class="dashboard-donut-chart"></div>
                        </div>
                    </div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
        </div>
        

<div class="modal fade" id="deleteDevice" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Are you sure?</h4>
                        </div>
                        <div class="modal-body">
                                <form>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceidUp" class="form-control" readonly>
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="password" id="delPass" class="form-control"
                                                        required>
                                                        <label class="form-label">Device Password</label>
                                                    </div>
                                                </div>   
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" onclick="deleteDevice()" >SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>


<div class="modal fade" id="changePassword" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Change Password</h4>
                        </div>
                        <div class="modal-body">
                                <form>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceidUp1" class="form-control" readonly>
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="password" id="oldPassword" class="form-control"
                                                        required>
                                                        <label class="form-label">Old Password</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="password" id="newPassword" class="form-control"
                                                        required>
                                                        <label class="form-label">New Password</label>
                                                    </div>
                                                </div>
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="password" id="confirmPassword" class="form-control"
                                                        required onblur="checkPasswords()">
                                                        <label class="form-label">Confirm Password</label>
                                                    </div>
                                                </div>   
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" id="PassButton" onclick="changePassword()" >SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>



        <div class="modal fade" id="updateDeviceName" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Update Device Name</h4>
                        </div>
                        <div class="modal-body">
                                <form>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceidUp2" class="form-control" readonly>
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" id="deviceN" class="form-control"
                                                        required>
                                                        <label class="form-label">Device Name</label>
                                                    </div>
                                                </div>
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" onclick="updateName()" >SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="modal fade" id="updateTerrif" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Update Electricity Unit</h4>
                        </div>
                        <div class="modal-body">
                                <form>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceidUp3" class="form-control" readonly>
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" id="ebTerif" class="form-control"
                                                        required>
                                                        <label class="form-label"><strong style="color:red">*</strong>assumption ₹9 per kHhr</label>
                                                    </div>
                                                </div>
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" onclick="updateTerrif()" >SAVE</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="modal fade" id="addDevice" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel" style="color:green">Add Device</h4>
                        </div>
                        <div class="modal-body">
                                <form>
                                        <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="devicename" class="form-control" required>
                                                    <label class="form-label" >Device Name</label>
                                                </div>
                                            </div>
            
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" id="deviceid" class="form-control">
                                                    <label class="form-label" required>Device Id</label>
                                                </div>
                                            </div>
                                            <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="password" id="devicepwd" class="form-control" required>
                                                        <label class="form-label">Device Password</label>
                                                    </div>
                                                </div>
                                        <br>
                                    </form>        
                        </div>
                        <div class="modal-footer">
                            <button type="button" style="color:green" class="btn btn-link waves-effect" onclick="addDevice()" >SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal" style="color:red">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave ?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <!-- <input type="text" style="display:none" id="kwhVal"> -->
                          </button>
                        </div>
                        <div class="modal-body">Select "LogOut" to end your current session.</div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-primary" href="../auth/src/authentication/logout.php">Logout</a>
                        </div>
                      </div>
                    </div>
                  </div>
    </section>

    <script src="plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
<!-- Bootstrap Core Js -->
<script src="plugins/bootstrap/js/bootstrap.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<!--Date Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.27/daterangepicker.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/gijgo@1.8.2/combined/js/gijgo.min.js" type="text/javascript"></script> -->
<!-- Select Plugin Js -->
<script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/morrisjs/morris.js"></script>
<!-- ChartJs -->
<script src="plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="plugins/flot-charts/jquery.flot.js"></script>
<script src="plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/main.js"></script>
<!-- Demo Js -->
<script src="js/demo.js"></script>
<script>
        $(document).ready(function () {
            var val = document.getElementById("realtime").value;
            console.log(val);
            var now = new Date();
            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);
            var today = (month) + "/" + (day) + "/" + now.getFullYear();
           // $('#selectdate').datepicker();
            $('#selectdate').val(today);
            getRealtimeData();
            viewDeviceData(null,null,null,null,null,null,null,null,null,null);
            getWeather();
        });   
        window.setInterval(function(){
            getRealtimeData();
         }, 20000);


        // $(document).click(function (e) {
        //  e.stopPropagation();
        //  var container = $(".dropdown");
        //  if (container.has(e.target).length === 0) {
        //    $('.dropdown').removeClass("open");  
        // }
        // });
 $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D') + '-' + end.format('MMMM D-YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        showDropdowns: true,
        linkedCalendars:false,
        maxDate: moment(),
        opens: 'center',
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);
});
$('#reportrange').blur(function(){
        console.log(($('#reportrange').data('daterangepicker').startDate._d).getDate());
    });

$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
  var startDate = picker.startDate;
  var endDate = picker.endDate;
  console.log(startDate);
  console.log(endDate);
  if(startDate.format('YYYY-MM-DD')==endDate.format('YYYY-MM-DD')){
    document.getElementById("selectdate").value = startDate.format('YYYY-MM-DD');
    viewDeviceData(null,null,null,null,null,null,null,null,null,null);
  } else {
    getDayMonthYearView(1,startDate.format('YYYY-MM-DD'),endDate.format('YYYY-MM-DD')); 
  }
});    
function checkPasswords(){
        var newPassword = document.getElementById("newPassword").value;
        var confirmPassword = document.getElementById("confirmPassword").value;
        if(newPassword=!confirmPassword){
            document.getElementById("confirmPassword").style.borderColor = "#ff0000";
            document.getElementById("newPassword").style.borderColor = "#ff0000";
            $("#PassButton").prop("disabled",true);  
        } else {
            document.getElementById("confirmPassword").style.borderColor = "#ffffff";
            document.getElementById("newPassword").style.borderColor = "#ffffff";
            $("#PassButton").prop("disabled",false);
        }
 }
    </script>
</body>
</html>